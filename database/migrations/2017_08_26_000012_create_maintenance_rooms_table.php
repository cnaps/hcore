<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaintenanceRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenance_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->text('note');
            $table->dateTime('available_time');
            $table->boolean('fixed');
            $table->integer('room_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('room_id')
                ->references('id')
                ->on('rooms');
            $table->foreign('member_id')
                ->references('id')
                ->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maintenance_rooms');
    }
}
