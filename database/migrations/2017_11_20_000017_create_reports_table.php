<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->integer('unit_price');
            $table->date('date');
            $table->string('unused_room_num');
            $table->text('note');
            $table->integer('ordered_rooms_quantity');
            $table->integer('cleaned_rooms_quantity');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('place_id')
                ->references('id')
                ->on('places');
            $table->foreign('member_id')
                ->references('id')
                ->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports');
    }
}
