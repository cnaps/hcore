<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('last_name_kana');
            $table->string('first_name_kana');
            $table->string('tel');
            $table->string('tel_mp');
            $table->string('postal_code');
            $table->string('pref');
            $table->string('address1');
            $table->string('address2');
            $table->string('building');
            $table->date('first_day');
            $table->date('final_day');
            $table->integer('status_id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->integer('place_id2')->unsigned();
            $table->integer('base_salary');
            $table->integer('traveling_expenses');
            $table->integer('hourly_wage');
            $table->integer('allowances');
            $table->integer('paid_vacation');
            $table->string('pass');
            $table->boolean('working');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('shift_monday');
            $table->integer('shift_tuesday');
            $table->integer('shift_wednesday');
            $table->integer('shift_thursday');
            $table->integer('shift_friday');
            $table->integer('shift_saturday');
            $table->integer('shift_sunday');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses');
            $table->foreign('place_id')
                ->references('id')
                ->on('places');
            $table->foreign('place_id2')
                ->references('id')
                ->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
