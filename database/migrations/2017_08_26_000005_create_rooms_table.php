<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('floor');
            $table->integer('room');
            $table->integer('not_room');
            $table->integer('condition_id')->unsigned();
            $table->integer('place_id')->unsigned();
            $table->integer('room_type_id')->unsigned();
            $table->integer('v_num');
            $table->integer('h_num');
            $table->integer('v_span');
            $table->integer('h_span');
            $table->timestamps();

            $table->foreign('condition_id')
                ->references('id')
                ->on('conditions');
            $table->foreign('place_id')
                ->references('id')
                ->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rooms');
    }
}
