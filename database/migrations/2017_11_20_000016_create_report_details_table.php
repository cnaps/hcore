<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('place_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->date('date');
            $table->integer('working_flg');
            $table->integer('key_check');
            $table->integer('make_total');
            $table->integer('floor1');
            $table->integer('room_count1');
            $table->integer('floor2');
            $table->integer('room_count2');
            $table->integer('floor3');
            $table->integer('room_count3');
            $table->integer('floor4');
            $table->integer('room_count4');
            $table->integer('floor5');
            $table->integer('room_count5');
            $table->string('other');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('member_id')
                ->references('id')
                ->on('members');
            $table->foreign('place_id')
                ->references('id')
                ->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('report_details');
    }
}
