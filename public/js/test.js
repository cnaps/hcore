(function(){
    $("#DailyTimes").tableExport({
        formats: ["xlsx"],
        bootstrap: true
    });
    $("#Sheet").tableExport({
        formats: ["xlsx"],
        bootstrap: true
    });
})();
