<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    public function places()
    {
        return $this->hasMany('App\Place');
    }

    protected $fillable = [
        'name',
        'name_kana',
    ];
}
