<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Maintenance_room extends Model
{
    use SoftDeletes;
    public function room()
    {
        return $this->belongsTo('App\Room');
    }
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    protected $fillable = [
        'note',
        'available_time',
        'fixed',
        'room_id',
        'member_id',
    ];
}
