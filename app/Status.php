<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function members()
    {
        return $this->hasMany('App\Member');
    }

    protected $fillable = [
        'name',
    ];
}
