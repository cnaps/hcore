<?php

namespace App\Http\Controllers;

use App\Daily_time;
use App\Member;
use App\Place;
use App\Time;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TimesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $times = Time::all();
        return view('times.index', compact('times'));
    }

    public function create()
    {
        $members = Member::all();
        return view('times.create', compact('members'));
    }

    public function store(Request $request)
    {
        Time::create($request->all());
        if($request->in_out == "in"){
            $member = Member::findOrFail($request->member_id)->first();
            $member->working = 1;
            $member->save();
        }else{
            $member = Member::findOrFail($request->member_id)->first();
            $member->working = 0;
            $member->save();
        }
        return redirect('/times');
    }

    public function show($id)
    {
        $time = Time::findOrFail($id);
        return view('times.show', compact('time'));
    }

    public function edit($id)
    {
        $time = Time::findOrFail($id);
        $members = Member::all();
        return view('times.edit', compact('time','members'));
    }

    public function update(Request $request, $id)
    {
        $time = Time::findOrFail($id);
        $time -> update($request -> all());
        if($request->in_out == "in"){
            $member = Member::findOrFail($request->member_id)->first();
            $member->working = 1;
            $member->save();
        }else{
            $member = Member::findOrFail($request->member_id)->first();
            $member->working = 0;
            $member->save();
        }
        return redirect("/times");
    }

    public function destroy($id)
    {
        $time = Time::findOrFail($id);
        $time->delete();
        return redirect('/times');
    }

    public function indexTimeCard()
    {
        $place_id = Auth::user()->place_id;
        $place = Place::All()->where('id', $place_id)->first();
        $members = Member::All()->where('place_id', $place_id)->sortBy('last_name_kana');

        return view('times.index_time_card', compact( 'members','place'));
    }

    public function createTimeCard($member_id)
    {
        $member = Member::where('id', $member_id)->first();
        return view('times.create_time_card', compact('member'));
    }

    public function createPass($member_id)
    {
        $member = Member::where('id', $member_id)->first();

        return view('times.create_pass', compact( 'member','member_id'));
    }

    public function storePass(Request $request, $id)
    {
        $this->validate($request, [
            'pass' => 'required|numeric|confirmed|digits:8',
        ],[
            'pass.required' => ':attributeが入力されていません',
            'pass.numeric' => ':attributeは数字のみ使用できます',
            'pass.confirmed' => '再入力のものと暗証番号が一致しません',
        ],[
            'pass' => '暗証番号',
        ]);
        $member = Member::findOrFail($id);
        $member->pass = $request->pass;
        $member->save();
        return redirect()->route('input_pass', [$id]);
    }

    public function changePass($member_id)
    {
        $member = Member::where('id', $member_id)->first();

        return view('times.change_pass', compact( 'member','member_id'));
    }

    public function updatePass(Request $request, $id)
    {
        $this->validate($request, [
            'pass_old' => 'required|exists:members,pass,id,'.$id,
            'pass' => 'required|numeric|confirmed|digits:8',
        ],[
            'pass_old.exists' => ':attributeが間違っています',
            'pass_old.required' => ':attributeが入力されていません',
            'pass.required' => ':attributeが入力されていません',
            'pass.numeric' => ':attributeは数字のみ使用できます',
            'pass.confirmed' => ':attributeが再入力された番号と一致しません',
        ],[
            'pass_old' => '暗証番号',
            'pass' => '新しい暗証番号',
        ]);
        $member = Member::findOrFail($id);
        $member->pass = $request->pass;
        $member->save();
        return redirect()->route('input_pass', [$id]);
    }

    public function inputPass($id)
    {
        $member = Member::findOrFail($id);
        $pass = strval($member->pass);
        if (strcmp($pass, "0") == 0){
            return redirect()->route('create_pass', [$id]);
        }else{
            return view('times.input_pass', compact( 'id'));
        }
    }

    public function checkPass(Request $request)
    {
        $id = $request->id;
        $this->validate($request, [
            'pass' => 'required|exists:members,pass,id,'.$id,
        ],[
            'pass.required' => ':attributeが入力されていません',
            'pass.exists' => ':attributeが間違っています',
        ],[
            'pass' => '暗証番号',
        ]);

        $member = Member::findOrFail($request->id);
        $p1 = $member->pass;
        $p2 = $request->pass;

        if($p1 == $p2){
            return redirect()->route('create_time_card', [$id]);
        }else{
            return redirect()->route('input_pass', [$id]);
        }

    }

    public function storeTimeCard(Request $request)
    {
        Time::create($request->all());
        if($request->in_out == "in"){
            $member = Member::findOrFail($request->member_id);
            $member->working = 1;
            $member->save();
        }else{
            $member = Member::findOrFail($request->member_id);
            $member->working = 0;
            $member->save();

            $time = Time::where('in_out', "in")->latest('created_at')->first();
            $out = new Carbon();;
            $in = new Carbon($time->created_at);
            $diff_min = $out->diffInMinutes($in);
            $daily_time = new Daily_time;
            $daily_time->member_id = $request->member_id;
            $daily_time->time = $diff_min;
            $daily_time->date = $out->format('Y-m-d');
            $daily_time->save();
        }
        return redirect('/time_card');
    }
}
