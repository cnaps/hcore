<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Member;
use App\Place;
use App\Room;
use App\Task;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlacesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $places = Place::all();
        return view('places.index', compact('places'));
    }

    public function create()
    {
        $hotels = Hotel::all();
        return view('places.create', compact('hotels'));
    }

    public function store(Request $request)
    {
        Place::create($request->all());
        return redirect('/places');
    }

    public function show($id)
    {
        $place = Place::findOrFail($id);
        return view('places.show', compact('place'));
    }

    public function edit($id)
    {
        $place = Place::findOrFail($id);
        $hotels = Hotel::all();
        return view('places.edit', compact('place','hotels'));
    }

    public function update(Request $request, $id)
    {
        $place = Place::findOrFail($id);
        $place -> update($request -> all());
        return redirect("/places");
    }

    public function destroy($id)
    {
        $place = Place::findOrFail($id);
        $place->delete();
        return redirect('/places');
    }

    public function indexSales()
    {
        $places = Place::all();
        $room_counts = Room::
            select('place_id',DB::raw('COUNT(*) AS count'))
            ->groupby('place_id')
            ->get();
        $rooms = Room::all();
        $today = Carbon::now();
        $today = date_format($today,'Y-m-d');
        $oneYearAgo = Carbon::now()->subYear(1);
        $oneYearAgo = date_format($oneYearAgo, 'Y-m-d');
        $oneMonthAgo = Carbon::now()->subMonth(1);
        $oneMonthAgo = date_format($oneMonthAgo, 'Y-m-d');
        $yesterday = $dt = Carbon::yesterday();
        $yesterday = date_format($yesterday, 'Y-m-d');
        $oneYearTasks = DB::table('tasks')
            ->whereBetween('done_date', [$oneYearAgo,$today])
            ->get();
        $oneMonthTasks = DB::table('tasks')
            ->whereBetween('done_date', [$oneMonthAgo,$today])
            ->get();
        $yesterdayTasks = DB::table('tasks')
            ->where('done_date', $yesterday)
            ->get();

        $members = Member::all();
        return view('places.index_sales', compact('places','room_counts','oneYearTasks','today','oneYearAgo','rooms','oneMonthAgo','yesterdayTasks','yesterday','oneMonthTasks','members'));
    }

    public function map()
    {
        $places = Place::all();
        $room_counts = Room::
        select('place_id',DB::raw('COUNT(*) AS count'))
            ->groupby('place_id')
            ->get();
        $rooms = Room::all();
        $today = Carbon::now();
        $today = date_format($today,'Y-m-d');
        $oneYearAgo = Carbon::now()->subYear(1);
        $oneYearAgo = date_format($oneYearAgo, 'Y-m-d');
        $oneMonthAgo = Carbon::now()->subMonth(1);
        $oneMonthAgo = date_format($oneMonthAgo, 'Y-m-d');
        $yesterday = $dt = Carbon::yesterday();
        $yesterday = date_format($yesterday, 'Y-m-d');
        $oneYearTasks = DB::table('tasks')
            ->whereBetween('done_date', [$oneYearAgo,$today])
            ->get();
        $oneMonthTasks = DB::table('tasks')
            ->whereBetween('done_date', [$oneMonthAgo,$today])
            ->get();
        $yesterdayTasks = DB::table('tasks')
            ->where('done_date', $yesterday)
            ->get();

        $members = Member::all();
        return view('places.map', compact('places','room_counts','oneYearTasks','today','oneYearAgo','rooms','oneMonthAgo','yesterdayTasks','yesterday','oneMonthTasks','members'));
    }
}
