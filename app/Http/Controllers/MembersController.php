<?php

namespace App\Http\Controllers;

use App\Member;
use App\Place;
use App\Status;
use App\User;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $members = Member::all()->sortBy('last_name_kana');
        $places = Place::all();
        return view('members.index', compact('members','places'));
    }

    public function create()
    {
        $statuses = Status::all();
        $places = Place::all();
        $users = User::all();
        return view('members.create', compact('statuses','places','users'));
    }

    public function store(Request $request)
    {
        Member::create($request->all());
        return redirect('/members');
    }

    public function show($id)
    {
        $member = Member::findOrFail($id);
        $places = Place::all();
        return view('members.show', compact('member','places'));
    }

    public function edit($id)
    {
        $member = Member::findOrFail($id);
        $statuses = Status::all();
        $places = Place::all();
        $users = User::all();
        return view('members.edit', compact('member','statuses','places','users'));
    }

    public function update(Request $request, $id)
    {
        $member = Member::findOrFail($id);
        $member -> update($request -> all());
        return redirect("/members");
    }

    public function destroy($id)
    {
        $member = Member::findOrFail($id);
        $member->delete();
        return redirect('/members');
    }

    public function indexShift($place_id)
    {
        $places = Place::all();
        $place_id = intval($place_id);
        $members = Member::all()->where('place_id',$place_id)->sortBy('last_name_kana');
        return view('members.index_shift', compact('members','place_id','places'));
    }

    public function updateShift(Request $request)
    {
        for ($i = 1;$i<=$request->input('count');$i++){
            $member = Member::find($request->input('member_id'.$i));
            $member->shift_monday = $request->input('monday'.$i);
            $member->shift_tuesday = $request->input('tuesday'.$i);
            $member->shift_wednesday = $request->input('wednesday'.$i);
            $member->shift_thursday = $request->input('thursday'.$i);
            $member->shift_friday = $request->input('friday'.$i);
            $member->shift_saturday = $request->input('saturday'.$i);
            $member->shift_sunday = $request->input('sunday'.$i);
            $member->save();
        }
        return redirect()->route('index_shift', [$request->input('place_id')]);
    }
}
