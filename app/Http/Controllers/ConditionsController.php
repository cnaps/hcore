<?php

namespace App\Http\Controllers;

use App\Condition;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ConditionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $conditions = Condition::all();
        return view('conditions.index', compact('conditions'));
    }
    
    public function create()
    {
        return view('conditions.create');
    }

    public function store(Request $request)
    {
        Condition::create($request->all());
        return redirect('/conditions');
    }

    public function show($id)
    {
        $condition = Condition::findOrFail($id);
        return view('conditions.show', compact('condition'));
    }

    public function edit($id)
    {
        $condition = Condition::findOrFail($id);
        return view('conditions.edit', compact('condition'));
    }

    public function update(Request $request, $id)
    {
        $condition = Condition::findOrFail($id);
        $condition -> update($request -> all());
        return redirect("/conditions");
    }

    public function destroy($id)
    {
        $condition = Condition::findOrFail($id);
        $condition->delete();
        return redirect('/conditions');
    }
}
