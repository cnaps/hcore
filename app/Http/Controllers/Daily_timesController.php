<?php

namespace App\Http\Controllers;

use App\Daily_time;
use App\Member;
use App\Place;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Daily_timesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $daily_times = Daily_time::all();
        return view('daily_times.index', compact('daily_times'));
    }

    public function create()
    {
        $members = Member::all();
        return view('daily_times.create', compact('members'));
    }

    public function store(Request $request)
    {
        Daily_time::create($request->all());
        return redirect('/daily_times');
    }

    public function show($id)
    {
        $daily_time = Daily_time::findOrFail($id);
        return view('daily_times.show', compact('daily_time'));
    }

    public function edit($id)
    {
        $daily_time = Daily_time::findOrFail($id);
        $members = Member::all();
        return view('daily_times.edit', compact('daily_time','members'));
    }

    public function update(Request $request, $id)
    {
        $daily_time = Daily_time::findOrFail($id);
        $daily_time -> update($request -> all());
        return redirect("/daily_times");
    }

    public function destroy($id)
    {
        $daily_time = Daily_time::findOrFail($id);
        $daily_time->delete();
        return redirect('/daily_times');
    }

    public function indexAllTimes()
    {
        $place_id = Auth::user()->place_id;
        $members = Member::all()->where('place_id',$place_id)->sortBy('last_name_kana');
        $places = Place::all();

        $month = date_format(Carbon::now(),'Y-m');

        $daily_times = DB::table('daily_times')
            ->select('member_id',DB::raw('SUM(time) as total_time'))
            ->whereRaw("DATE_FORMAT(date, '%Y-%m') = '".$month."'")
            ->groupBy('member_id')
            ->get();
        $day_counts = DB::table('daily_times')
            ->select('member_id',DB::raw('DATE_FORMAT(date, \'%Y-%m-%d\') AS date, COUNT(*) AS date_count'))
            ->whereRaw("DATE_FORMAT(date, '%Y-%m') = '".$month."'")
            ->groupBy('member_id')
            ->groupBy(DB::raw('DATE_FORMAT(date, \'%Y%m%d\')'))
            ->get();

        return view('daily_times.index_all_times', compact('daily_times','month','members','day_counts','places','place_id'));
    }

    public function indexAllTimes2(Request $request)
    {
        $place_id = $request->input('place');
        $members = Member::all()->where('place_id',$place_id)->sortBy('last_name_kana');
        $places = Place::all();

        $month = $request->input('month');
        $daily_times = DB::table('daily_times')
            ->select('member_id',DB::raw('SUM(time) as total_time'))
            ->whereRaw("DATE_FORMAT(date, '%Y-%m') = '".$month."'")
            ->groupBy('member_id')
            ->get();
        $day_counts = DB::table('daily_times')
            ->select('member_id',DB::raw('DATE_FORMAT(date, \'%Y-%m-%d\') AS date, COUNT(*) AS date_count'))
            ->whereRaw("DATE_FORMAT(date, '%Y-%m') = '".$month."'")
            ->groupBy('member_id')
            ->groupBy(DB::raw('DATE_FORMAT(date, \'%Y%m%d\')'))
            ->get();

        return view('daily_times.index_all_times2', compact('daily_times','month','members','day_counts','places','place_id'));
    }
}
