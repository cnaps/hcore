<?php

namespace App\Http\Controllers;

use App\Member;
use App\Note;
use App\Place;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $notes = Note::all();
        return view('notes.index', compact('notes'));
    }

    public function create()
    {
        $places = Place::all();
        $members = Member::all();
        return view('notes.create', compact('places','members'));
    }

    public function store(Request $request)
    {
        Note::create($request->all());
        return redirect('/notes');
    }

    public function show($id)
    {
        $note = Note::findOrFail($id);
        return view('notes.show', compact('note'));
    }

    public function edit($id)
    {
        $note = Note::findOrFail($id);
        $places = Place::all();
        $members = Member::all();
        return view('notes.edit', compact('note','places','members'));
    }

    public function update(Request $request, $id)
    {
        $note = Note::findOrFail($id);
        $note -> update($request -> all());
        return redirect("/notes");
    }

    public function destroy($id)
    {
        $note = Note::findOrFail($id);
        $note->delete();
        return redirect('/notes');
    }

    public function createMasterNote()
    {
        $user_id = Auth::user()->id;
        $place_id = Auth::user()->place_id;

        $members = Member::All()->where('user_id', $user_id)->sortBy('last_name_kana');

        $places = Place::all()->where('id',$place_id);
        return view('notes.create_master_note', compact('places','members'));
    }

    public function storeMasterNote(Request $request)
    {
        Note::create($request->all());
        return redirect()->route('index_note');
    }

    public function deleteMasterNote($id)
    {
        $note = Note::findOrFail($id);
        $note->delete();
        return redirect()->route('index_note');
    }
}
