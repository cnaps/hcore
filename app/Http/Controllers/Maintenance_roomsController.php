<?php

namespace App\Http\Controllers;

use App\Maintenance_room;
use App\Member;
use App\Room;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Maintenance_roomsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $maintenance_rooms = Maintenance_room::all();
        return view('maintenance_rooms.index', compact('maintenance_rooms'));
    }
    
    public function create()
    {
        $rooms = Room::all();
        $members = Member::all();
        return view('maintenance_rooms.create', compact('rooms','members'));
    }

    public function store(Request $request)
    {
        $mr = new Maintenance_room;
        $mr->note = $request->note;
        $mr->fixed = $request->fixed;
        $mr->room_id = $request->room_id;
        $mr->member_id = $request->member_id;
        $mr->save();

        return redirect('/maintenance_rooms');
    }

    public function show($id)
    {
        $maintenance_room = Maintenance_room::findOrFail($id);
        return view('maintenance_rooms.show', compact('maintenance_room'));
    }
    
    public function edit($id)
    {
        $maintenance_room = Maintenance_room::findOrFail($id);
        $rooms = Room::all();
        $members = Member::all();
        return view('maintenance_rooms.edit', compact('maintenance_room','rooms','members'));
    }

    public function update(Request $request, $id)
    {
        $maintenance_room = Maintenance_room::findOrFail($id);
        $maintenance_room -> update($request -> all());
        return redirect("/maintenance_rooms");
    }

    public function destroy($id)
    {
        $maintenance_room = Maintenance_room::findOrFail($id);
        $maintenance_room->delete();
        return redirect('/maintenance_rooms');
    }
}
