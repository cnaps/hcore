<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $statuses = Status::all();
        return view('statuses.index', compact('statuses'));
    }

    public function create()
    {
        return view('statuses.create');
    }

    public function store(Request $request)
    {
        Status::create($request->all());
        return redirect('/statuses');
    }

    public function show($id)
    {
        $status = Status::findOrFail($id);
        return view('statuses.show', compact('status'));
    }

    public function edit($id)
    {
        $status = Status::findOrFail($id);
        return view('statuses.edit', compact('status'));
    }

    public function update(Request $request, $id)
    {
        $status = Status::findOrFail($id);
        $status -> update($request -> all());
        return redirect("/statuses");
    }

    public function destroy($id)
    {
        $status = Status::findOrFail($id);
        $status->delete();
        return redirect('/statuses');
    }
}
