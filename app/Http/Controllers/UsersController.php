<?php

namespace App\Http\Controllers;

use App\Authority;
use App\Place;
use App\User;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        $authorities = Authority::all();
        $places = Place::all();
        return view('users.index', compact('users','authorities','places'));
    }

    public function create()
    {
        $authorities = Authority::all();
        $places = Place::all();
        return view('auth.register',compact('authorities','places'));
    }

    public function store(Request $request)
    {
        $request->merge(['password' => Hash::make($request->password)]);
        User::create($request->all());
        return redirect('/users');
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $authorities = Authority::all();
        $places = Place::all();
        return view('users.edit', compact('user','authorities','places'));
    }

    public function editPass($id)
    {
        $user = User::findOrFail($id);
        $authorities = Authority::all();
        $places = Place::all();
        return view('users.edit_pass', compact('user','authorities','places'));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user -> update($request -> all());
        return redirect("/users");
    }

    public function updatePass(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $request->merge(['password' => Hash::make($request->password)]);
        $user->password = $request->password;
        $user->save();
        return redirect("/users");
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/users');
    }
}
