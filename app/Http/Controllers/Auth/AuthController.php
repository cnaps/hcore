<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/index_note';
    protected $loginPath = '/login';

    public function authenticated()
    {
        if(Auth::check()) {
            if(Auth::user()->authority_id == 1) {
                return redirect('/index_note');
            } else {
                return redirect('/create_daily_report');
            }
        }
    }

    public function __construct()
    {
//        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^[!-~]+$/',
        ],[
            'email.exists' => '登録許可された:attributeを入力してください',
            'password.regex' => ':attributeは英数字のみを使用してください'
        ],[
            'name' => '名前',
            'email' => 'メールアドレス',
            'password' => 'パスワード',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
