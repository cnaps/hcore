<?php

namespace App\Http\Controllers;

use App\Member;
use App\Note;
use App\Place;
use App\Reply;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $replies = Reply::all();
        return view('replies.index', compact('replies'));
    }
    
    public function create()
    {
        $notes = Note::all();
        $members = Member::all();
        return view('replies.create', compact('notes','members'));
    }
    
    public function store(Request $request)
    {
        Reply::create($request->all());
        return redirect('/replies');
    }

    public function show($id)
    {
        $reply = Reply::findOrFail($id);
        return view('replies.show', compact('reply'));
    }

    public function edit($id)
    {
        $reply = Reply::findOrFail($id);
        $notes = Note::all();
        $members = Member::all();
        return view('replies.edit', compact('reply','notes','members'));
    }

    public function update(Request $request, $id)
    {
        $reply = Reply::findOrFail($id);
        $reply -> update($request -> all());
        return redirect("/replies");
    }

    public function destroy($id)
    {
        $reply = Reply::findOrFail($id);
        $reply->delete();
        return redirect('/replies');
    }

    public function showNote($id)
    {
        $place_id = Auth::user()->place_id;
        $note = Note::find($id);
        $member = Member::find($note->member_id);
        $place = Place::find($note->place_id);
        $replies = Reply::all()->where('note_id',$id)->sortByDesc('created_at');
        return view('replies.show_note', compact('replies','note','member','place', 'auth_member','place_id'));
    }

    public function createNote($id)
    {
        $user_id = Auth::user()->id;

        $members = Member::All()->where('user_id', $user_id)->sortBy('last_name_kana');
        $note = Note::find($id);
        $member = Member::find($note->member_id);
        $place = Place::find($note->place_id);
        $replies = Reply::all()->where('note_id',$id)->sortByDesc('created_at');
        return view('replies.create_note', compact('replies','note','member','place','members','id'));
    }

    public function storeNote(Request $request)
    {
        Reply::create($request->all());
        return redirect()->route('show_note', [$request->note_id]);
    }

    public function indexNote()
    {
        if(Auth::check()) {
            if(Auth::user()->authority_id == 4) {
                return redirect('/create_daily_report');
            }
        }
        $place_id = Auth::user()->place_id;
        $notes1 = Note::where('place_id',$place_id);
        $notes = Note::where('place_id',1)->union($notes1)->get()->sortByDesc('created_at');
        return view('replies.index_note', compact('notes'));
    }

    public function deleteNote($id)
    {
        $reply = Reply::findOrFail($id);
        $note_id = $reply->note_id;
        $reply->delete();
        return redirect()->route('show_note', [$note_id]);
    }
}
