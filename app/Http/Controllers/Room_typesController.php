<?php

namespace App\Http\Controllers;

use App\Room_type;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Room_typesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $room_types = Room_type::all();
        return view('room_types.index', compact('room_types'));
    }

    public function create()
    {
        $room_types = Room_type::all();
        return view('room_types.create', compact('room_types'));
    }

    public function store(Request $request)
    {
        Room_type::create($request->all());
        return redirect('/room_types');
    }

    public function show($id)
    {
        $room_type = Room_type::findOrFail($id);
        return view('room_types.show', compact('room_type'));
    }

    public function edit($id)
    {
        $room_type = Room_type::findOrFail($id);
        return view('room_types.edit', compact('room_type'));
    }

    public function update(Request $request, $id)
    {
        $room_type = Room_type::findOrFail($id);
        $room_type -> update($request -> all());
        return redirect("/room_types");
    }

    public function destroy($id)
    {
        $room_type = Room_type::findOrFail($id);
        $room_type->delete();
        return redirect('/room_types');
    }
}
