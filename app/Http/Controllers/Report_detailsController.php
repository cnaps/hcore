<?php

namespace App\Http\Controllers;

use App\Member;
use App\Place;
use App\Report_detail;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Report_detailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $report_details = Report_detail::all();
        return view('report_details.index', compact('report_details'));
    }

    public function create()
    {
        $places = Place::all();
        $members = Member::all();
        return view('report_details.create', compact('places','members'));
    }

    public function store(Request $request)
    {
        Report_detail::create($request->all());
        return redirect('/report_details');
    }

    public function show($id)
    {
        $report_detail = Report_detail::findOrFail($id);
        return view('report_details.show', compact('report_detail'));
    }

    public function edit($id)
    {
        $report_detail = Report_detail::findOrFail($id);
        $places = Place::all();
        $members = Member::all();
        return view('report_details.edit', compact('report_detail','places','members'));
    }

    public function update(Request $request, $id)
    {
        $report_details = Report_detail::findOrFail($id);
        $report_details -> update($request -> all());
        return redirect("/report_details");
    }

    public function destroy($id)
    {
        $report_detail = Report_detail::findOrFail($id);
        $report_detail->delete();
        return redirect('/report_details');
    }
}
