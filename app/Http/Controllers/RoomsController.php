<?php

namespace App\Http\Controllers;

use App\Condition;
use App\Member;
use App\Place;
use App\Room;
use App\Task;
use App\Room_type;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RoomsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $room_types = Room_type::all();
        $rooms = Room::all();
        return view('rooms.index', compact('rooms','room_types'));
    }

    public function create()
    {
        $room_types = Room_type::all();
        $conditions = Condition::all();
        $places = Place::all();
        return view('rooms.create', compact('conditions','places','room_types'));
    }

    public function store(Request $request)
    {
        for($i = 1;$i < 41;$i++) {
            if($request->input('room_'.$i) == 0){
                continue;
            }else{
                $room = new Room;
                $room->place_id = $request->place_id;
                $room->floor = $request->floor;
                $room->condition_id = 1;
                $room->room = $request->input('room_'.$i);
                $room->not_room = $request->input('not_room_'.$i);
                $room->room_type_id = $request->input('room_type_id_'.$i);
                $room->v_num = $request->input('v_num_'.$i);
                $room->h_num = $request->input('h_num_'.$i);
                $room->v_span = $request->input('v_span_'.$i);
                $room->h_span = $request->input('h_span_'.$i);
                $room->save();
            }
        }
        return redirect('/rooms');
    }

    public function show($id)
    {
        $room = Room::findOrFail($id);
        return view('rooms.show', compact('room'));
    }

    public function edit($id)
    {
        $room = Room::findOrFail($id);
        $conditions = Condition::all();
        $room_types = Room_type::all();
        $places = Place::all();
        return view('rooms.edit', compact('room','conditions','places','room_types'));
    }

    public function update(Request $request, $id)
    {
        $room = Room::findOrFail($id);
        $room -> update($request -> all());
        return redirect("/rooms");
    }

    public function destroy($id)
    {
        $room = Room::findOrFail($id);
        $room->delete();
        return redirect('/rooms');
    }

    public function indexRoom()
    {
        $place_id = Auth::user()->place_id;
        $members = Member::all()->where('place_id',$place_id);

        $date = date_format(Carbon::now(),'Y-m-d');
        $tasks = Task::where('done_date',$date)->get();

        $conditions = Condition::all();
        $max_floor = Room::all()->where('place_id',$place_id)->max('floor');
        $max_h_num = Room::all()->where('place_id',$place_id)->max('h_num');
        $max_h_num = $max_h_num + 1;
        $max_v_num = Room::all()->where('place_id',$place_id)->max('v_num');
        $max_v_num = $max_v_num + 1;
        $rooms = Room::all()->where('place_id',$place_id)->sortBy('h_num')->sortBy('v_num')->sortByDesc('floor');
        return view('rooms.index_room', compact('rooms','conditions','max_floor','max_h_num','max_v_num','members','tasks'));
    }

    public function createOrderRoom()
    {
        $place_id = Auth::user()->place_id;

        $date = date_format(Carbon::now(),'Y-m-d');
        $tasks = Task::where('done_date',$date)->get();

        $today = Carbon::now()->format('Y-m-d');
        $conditions = Condition::all();
        $max_floor = Room::all()->where('place_id',$place_id)->max('floor');
        $max_h_num = Room::all()->where('place_id',$place_id)->max('h_num');
        $max_h_num = $max_h_num + 1;
        $max_v_num = Room::all()->where('place_id',$place_id)->max('v_num');
        $max_v_num = $max_v_num + 1;
        $rooms = Room::all()->where('place_id',$place_id)->sortBy('h_num')->sortBy('v_num')->sortByDesc('floor');
        return view('rooms.create_order_room', compact('rooms','conditions','max_floor','max_h_num','max_v_num','tasks','today'));
    }

    public function createOrderRoom2($date)
    {
        $place_id = Auth::user()->place_id;

        $tasks = Task::where('done_date',$date)->get();

        $conditions = Condition::all();
        $max_floor = Room::all()->where('place_id',$place_id)->max('floor');
        $max_h_num = Room::all()->where('place_id',$place_id)->max('h_num');
        $max_h_num = $max_h_num + 1;
        $max_v_num = Room::all()->where('place_id',$place_id)->max('v_num');
        $max_v_num = $max_v_num + 1;
        $rooms = Room::all()->where('place_id',$place_id)->sortBy('h_num')->sortBy('v_num')->sortByDesc('floor');
        return view('rooms.create_order_room2', compact('rooms','conditions','max_floor','max_h_num','max_v_num','tasks','date'));
    }

    public function indexFinishRoom()
    {
        $place_id = Auth::user()->place_id;

        $date = date_format(Carbon::now(),'Y-m-d');
        $tasks = Task::where('done_date',$date)->get();

        $today = Carbon::now()->format('Y-m-d');
        $conditions = Condition::all();
        $max_floor = Room::all()->where('place_id',$place_id)->max('floor');
        $max_h_num = Room::all()->where('place_id',$place_id)->max('h_num');
        $max_h_num = $max_h_num + 1;
        $max_v_num = Room::all()->where('place_id',$place_id)->max('v_num');
        $max_v_num = $max_v_num + 1;
        $rooms = Room::all()->where('place_id',$place_id)->sortBy('h_num')->sortBy('v_num')->sortByDesc('floor');
        return view('rooms.index_finish_room', compact('rooms','conditions','max_floor','max_h_num','max_v_num','tasks','today'));
    }

    public function tmpRoomInput()
    {
        return view('rooms.tmp_room_input');
    }
}
