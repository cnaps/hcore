<?php

namespace App\Http\Controllers;

use App\Member;
use App\Place;
use App\Report;
use App\Report_detail;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $reports = Report::all();
        return view('reports.index', compact('reports'));
    }

    public function create()
    {
        $places = Place::all();
        $members = Member::all();
        return view('reports.create', compact('places','members'));
    }

    public function store(Request $request)
    {
        Report::create($request->all());
        return redirect('/reports');
    }

    public function show($id)
    {
        $report = Report::findOrFail($id);
        return view('reports.show', compact('report'));
    }

    public function edit($id)
    {
        $report = Report::findOrFail($id);
        $places = Place::all();
        $members = Member::all();
        return view('reports.edit', compact('report','places','members'));
    }

    public function update(Request $request, $id)
    {
        $report = Report::findOrFail($id);
        $report -> update($request -> all());
        return redirect("/reports");
    }

    public function destroy($id)
    {
        $report = Report::findOrFail($id);
        $report->delete();
        return redirect('/reports');
    }

    public function indexDailyReport()
    {
        $week = array('日', '月', '火', '水', '木', '金', '土');

        $date = Carbon::now();
        $dayCount = $date->daysInMonth;
        $date_m = $date->format('Y-m');
        $startDay = $date->startOfMonth()->format('Y-m-d');
        $endDay = $date->endOfMonth()->format('Y-m-d');

        $place_id = 1;

        $month = $date->month;
        $reports = DB::table('reports')
            ->whereMonth('date', '=', $month)
            ->where('place_id',$place_id)
            ->orderBy('date')
            ->get();
        $report_details = DB::table('report_details')
            ->whereMonth('date', '=', $month)
            ->where('place_id',$place_id)
            ->orderBy('date')
            ->get();

        $report_details_member1 = DB::table('members')
            ->leftJoin('users', 'members.user_id', '=', 'users.id')
            ->where(function ($query) use($place_id) {
                $query->where('users.place_id',$place_id)
                    ->where('members.deleted_at','=',null);
            })
            ->select('members.id','members.last_name','members.last_name_kana')
            ->groupBy('members.id');

        $report_details_member = DB::table('report_details')
            ->leftJoin('members', 'report_details.member_id', '=', 'members.id')
            ->where('report_details.date', '>=', $startDay)
            ->where(function ($query) use($startDay,$endDay,$place_id) {
                $query->where('report_details.date', '>=', $startDay)
                    ->where('report_details.date', '<=', $endDay)
                    ->where('report_details.place_id',$place_id);
            })
            ->select('report_details.member_id','members.last_name','members.last_name_kana')
            ->union($report_details_member1)
            ->groupBy('members.id')
            ->orderBy('last_name_kana')
            ->get();

        $place2 = Place::All()->where('id', $place_id)->first();
        $places = Place::All();
        $members = Member::All();

        return view('reports.index_daily_report', compact('reports','report_details','report_details_member','place2','places','members','date','place_id','today','date_f','week','date_m','dayCount','month'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexDailyReport2(Request $request)
    {
        $week = array('日', '月', '火', '水', '木', '金', '土');


        if ($request->input('month') == null){
            $date = Carbon::now();
            $dayCount = $date->daysInMonth;
            $date_m = $date->format('Y-m');
        }else{
            $date = $request->input('month');
            $date_m = $date;
            $date = $date."-01";
            $date = Carbon::parse($date);
            $dayCount = $date->daysInMonth;
        }
        $startDay = $date->startOfMonth()->format('Y-m-d');
        $endDay = $date->endOfMonth()->format('Y-m-d');


        if ($request->input('place_id') == null){
            $place_id = 1;
        }else{
            $place_id = $request->input('place_id');
        }
        $month = $date->month;
        $reports = DB::table('reports')
            ->whereMonth('date', '=', $month)
            ->where('place_id',$place_id)
            ->orderBy('date')
            ->get();
        $report_details = DB::table('report_details')
            ->whereMonth('date', '=', $month)
            ->where('place_id',$place_id)
            ->orderBy('date')
            ->get();

        $report_details_member1 = DB::table('members')
            ->leftJoin('users', 'members.user_id', '=', 'users.id')
            ->where(function ($query) use($place_id) {
                $query->where('users.place_id',$place_id)
                    ->where('members.deleted_at','=',null);
            })
            ->select('members.id','members.last_name','members.last_name_kana')
            ->groupBy('members.id');

        $report_details_member = DB::table('report_details')
            ->leftJoin('members', 'report_details.member_id', '=', 'members.id')
            ->where('report_details.date', '>=', $startDay)
            ->where(function ($query) use($startDay,$endDay,$place_id) {
                $query->where('report_details.date', '>=', $startDay)
                    ->where('report_details.date', '<=', $endDay)
                    ->where('report_details.place_id',$place_id);
            })
            ->select('report_details.member_id','members.last_name','members.last_name_kana')
            ->union($report_details_member1)
            ->groupBy('members.id')
            ->orderBy('last_name_kana')
            ->get();

        $place2 = Place::All()->where('id', $place_id)->first();
        $places = Place::All();
        $members = Member::All();

        return view('reports.index_daily_report', compact('reports','report_details','report_details_member','place2','places','members','date','place_id','today','date_f','week','date_m','dayCount','month'));
    }

    public function createDailyReport()
    {
        $today = Carbon::now()->format('Y年m月d日');
        $week = array('日', '月', '火', '水', '木', '金', '土');
        $day = Carbon::now()->dayOfWeek;
        $today = $today."（".$week[$day]."）";
        $today2 = Carbon::now()->format('Y-m-d');

        $place_id = Auth::user()->place_id;
        $place = Place::All()->where('id', $place_id)->first();
        $members = Member::All()->where('place_id2', $place_id)->sortBy('last_name_kana');
        $member_count = Member::All()->where('place_id2', $place_id)->count();

        $report = Report::All()->sortByDesc('created_at')->first();
        if ($report){
            $latest = $report->date;
            $latest = new Carbon($latest);
            // 今日かどうか
            if($latest->isToday()){
                return redirect('/edit_daily_report');
            }else{
                return view('reports.create_daily_report', compact('place','members','today','today2','member_count'));
            }
        }else{
            return view('reports.create_daily_report', compact('place','members','today','today2','member_count'));
        }
    }

    public function editDailyReport()
    {
        $today = Carbon::now()->format('Y年m月d日');
        $week = array('日', '月', '火', '水', '木', '金', '土');
        $day = Carbon::now()->dayOfWeek;
        $today = $today."（".$week[$day]."）";
        $today2 = Carbon::now()->format('Y-m-d');
        $place_id = Auth::user()->place_id;
        $report_details = Report_detail::where('place_id',$place_id)->where('date', $today2)->get();
        $place = Place::All()->where('id', $place_id)->first();
        $members = Member::All()->where('place_id2', $place_id)->sortBy('last_name_kana');
        $member_count = Member::All()->where('place_id2', $place_id)->count();
        $report = Report::All()->sortByDesc('created_at')->first();
        if ($report->date->format('Y-m-d') != $today2){
            return redirect('/create_daily_report');
        }
        return view('reports.edit_daily_report', compact('report','place','members','today','today2','report_details','member_count'));
    }

    public function checkDailyReport(Request $request)
    {
        $week = array('日', '月', '火', '水', '木', '金', '土');
        $day = $request->input('date');
        $day = new Carbon($day);
        $day = $day->dayOfWeek;
        $today = $request->input('date')."（".$week[$day]."）";

        $place = Place::find($request->input('place_id'));
//        $members = Member::All()->where('place_id', $request->input('place_id'))->sortBy('last_name_kana');
        $member_toban = Member::find($request->input('member_id'));

        for($i = 0;$i < $request->input('member_count');$i++) {
            $member_id = $request->input('member_id'.$i);
            if ($request->input('working_flg'.$i) == 1){
                $working_flg = 1;
            }else{
                $working_flg = 0;
            }
            if ($request->input('key_check'.$i) == 1){
                $key_check = 1;
            }else{
                $key_check = 0;
            }
            if ($request->input('make_total'.$i) == null){
                $make_total = 0;
            }else{
                $make_total = $request->input('make_total'.$i);
            }
            if ($request->input('floor1_'.$i) == null){
                $floor1 = 0;
            }else{
                $floor1 = $request->input('floor1_'.$i);
            }
            if ($request->input('room_count1_'.$i) == null){
                $room_count1 = 0;
            }else{
                $room_count1 = $request->input('room_count1_'.$i);
            }
            if ($request->input('floor2_'.$i) == null){
                $floor2 = 0;
            }else{
                $floor2 = $request->input('floor2_'.$i);
            }
            if ($request->input('room_count2_'.$i) == null){
                $room_count2 = 0;
            }else{
                $room_count2 = $request->input('room_count2_'.$i);
            }
            if ($request->input('floor3_'.$i) == null){
                $floor3 = 0;
            }else{
                $floor3 = $request->input('floor3_'.$i);
            }
            if ($request->input('room_count3_'.$i) == null){
                $room_count3 = 0;
            }else{
                $room_count3 = $request->input('room_count3_'.$i);
            }
            if ($request->input('floor4_'.$i) == null){
                $floor4 = 0;
            }else{
                $floor4 = $request->input('floor4_'.$i);
            }
            if ($request->input('room_count4_'.$i) == null){
                $room_count4 = 0;
            }else{
                $room_count4 = $request->input('room_count4_'.$i);
            }
            if ($request->input('floor5_'.$i) == null){
                $floor5 = 0;
            }else{
                $floor5 = $request->input('floor1_'.$i);
            }
            if ($request->input('room_count5_'.$i) == null){
                $room_count5 = 0;
            }else{
                $room_count5 = $request->input('room_count5_'.$i);
            }
            if ($request->input('other'.$i) == null){
                $other = "";
            }else{
                $other = $request->input('other'.$i);
            }
            $member = Member::find($member_id);
            $request2[$i] = array(
                'member_id' => $member_id,
                'member_name' => $member->last_name." ".$member->first_name,
                'working_flg' => $working_flg,
                'key_check' => $key_check,
                'make_total' => $make_total,
                'floor1' => $floor1,
                'room_count1' => $room_count1,
                'floor2' => $floor2,
                'room_count2' => $room_count2,
                'floor3' => $floor3,
                'room_count3' => $room_count3,
                'floor4' => $floor4,
                'room_count4' => $room_count4,
                'floor5' => $floor5,
                'room_count5' => $room_count5,
                'other' => $other,
            );
        }
        return view('reports.check_daily_report', compact('request', 'request2','place','day','week','today','member_toban'));
    }

    public function storeDailyReport(Request $request)
    {
        for($i = 0;$i < $request->input('member_count');$i++) {
            if($request->input('working_flg'.$i) == 0){
                continue;
            }else{
                $params = array(
                    "member_id"=>$request->input('member_id'.$i),
                    "date"=>$request->input('date'),
                );
                $report_detail = Report_detail::firstOrNew($params);
                $report_detail->place_id = $request->input('place_id');
                $report_detail->member_id = $request->input('member_id'.$i);
                $report_detail->date = $request->input('date');
                $report_detail->working_flg = $request->input('working_flg'.$i);
                if ($request->input('key_check'.$i) == 1){
                    $report_detail->key_check = 1;
                }else{
                    $report_detail->key_check = 0;
                }
                if ($request->input('make_total'.$i) == null){
                    $report_detail->make_total = 0;
                }else{
                    $report_detail->make_total = $request->input('make_total'.$i);
                }
                if ($request->input('floor1_'.$i) == null){
                    $report_detail->floor1 = 0;
                }else{
                    $report_detail->floor1 = $request->input('floor1_'.$i);
                }
                if ($request->input('room_count1_'.$i) == null){
                    $report_detail->room_count1 = 0;
                }else{
                    $report_detail->room_count1 = $request->input('room_count1_'.$i);
                }
                if ($request->input('floor2_'.$i) == null){
                    $report_detail->floor2 = 0;
                }else{
                    $report_detail->floor2 = $request->input('floor2_'.$i);
                }
                if ($request->input('room_count2_'.$i) == null){
                    $report_detail->room_count2 = 0;
                }else{
                    $report_detail->room_count2 = $request->input('room_count2_'.$i);
                }
                if ($request->input('floor3_'.$i) == null){
                    $report_detail->floor3 = 0;
                }else{
                    $report_detail->floor3 = $request->input('floor3_'.$i);
                }
                if ($request->input('room_count3_'.$i) == null){
                    $report_detail->room_count3 = 0;
                }else{
                    $report_detail->room_count3 = $request->input('room_count3_'.$i);
                }
                if ($request->input('floor4_'.$i) == null){
                    $report_detail->floor4 = 0;
                }else{
                    $report_detail->floor4 = $request->input('floor4_'.$i);
                }
                if ($request->input('room_count4_'.$i) == null){
                    $report_detail->room_count4 = 0;
                }else{
                    $report_detail->room_count4 = $request->input('room_count4_'.$i);
                }
                if ($request->input('floor5_'.$i) == null){
                    $report_detail->floor5 = 0;
                }else{
                    $report_detail->floor5 = $request->input('floor1_'.$i);
                }
                if ($request->input('room_count5_'.$i) == null){
                    $report_detail->room_count5 = 0;
                }else{
                    $report_detail->room_count5 = $request->input('room_count5_'.$i);
                }
                if ($request->input('other'.$i) == null){
                    $report_detail->other = "";
                }else{
                    $report_detail->other = $request->input('other'.$i);
                }
                $report_detail->save();
            }
        }

        Report::create($request->all());
        return redirect('/edit_daily_report')->with('status', '保存しました');
    }

    public function updateDailyReport(Request $request)
    {
        for($i = 0;$i < $request->input('member_count');$i++) {
            if($request->input('working_flg'.$i) == 0){
                $params = array(
                    "member_id"=>$request->input('member_id'.$i),
                    "place_id"=>$request->input('place_id'),
                    "date"=>$request->input('date'),
                );
                $report_detail = Report_detail::firstOrNew($params);
                $report_detail->delete();
                continue;
            }else{
                $params = array(
                    "member_id"=>$request->input('member_id'.$i),
                    "place_id"=>$request->input('place_id'),
                    "date"=>$request->input('date'),
                );
                $report_detail = Report_detail::firstOrNew($params);
                $report_detail->place_id = $request->input('place_id');
                $report_detail->member_id = $request->input('member_id'.$i);
                $report_detail->date = $request->input('date');
                $report_detail->working_flg = $request->input('working_flg'.$i);
                if ($request->input('key_check'.$i) == 1){
                    $report_detail->key_check = 1;
                }else{
                    $report_detail->key_check = 0;
                }
                if ($request->input('make_total'.$i) == null){
                    $report_detail->make_total = 0;
                }else{
                    $report_detail->make_total = $request->input('make_total'.$i);
                }
                if ($request->input('floor1_'.$i) == null){
                    $report_detail->floor1 = 0;
                }else{
                    $report_detail->floor1 = $request->input('floor1_'.$i);
                }
                if ($request->input('room_count1_'.$i) == null){
                    $report_detail->room_count1 = 0;
                }else{
                    $report_detail->room_count1 = $request->input('room_count1_'.$i);
                }
                if ($request->input('floor2_'.$i) == null){
                    $report_detail->floor2 = 0;
                }else{
                    $report_detail->floor2 = $request->input('floor2_'.$i);
                }
                if ($request->input('room_count2_'.$i) == null){
                    $report_detail->room_count2 = 0;
                }else{
                    $report_detail->room_count2 = $request->input('room_count2_'.$i);
                }
                if ($request->input('floor3_'.$i) == null){
                    $report_detail->floor3 = 0;
                }else{
                    $report_detail->floor3 = $request->input('floor3_'.$i);
                }
                if ($request->input('room_count3_'.$i) == null){
                    $report_detail->room_count3 = 0;
                }else{
                    $report_detail->room_count3 = $request->input('room_count3_'.$i);
                }
                if ($request->input('floor4_'.$i) == null){
                    $report_detail->floor4 = 0;
                }else{
                    $report_detail->floor4 = $request->input('floor4_'.$i);
                }
                if ($request->input('room_count4_'.$i) == null){
                    $report_detail->room_count4 = 0;
                }else{
                    $report_detail->room_count4 = $request->input('room_count4_'.$i);
                }
                if ($request->input('floor5_'.$i) == null){
                    $report_detail->floor5 = 0;
                }else{
                    $report_detail->floor5 = $request->input('floor5_'.$i);
                }
                if ($request->input('room_count5_'.$i) == null){
                    $report_detail->room_count5 = 0;
                }else{
                    $report_detail->room_count5 = $request->input('room_count5_'.$i);
                }
                if ($request->input('other'.$i) == null){
                    $report_detail->other = "";
                }else{
                    $report_detail->other = $request->input('other'.$i);
                }
                $report_detail->save();
            }
        }
        $params2 = array(
            "place_id"=>$request->input('place_id'),
            "date"=>$request->input('date'),
        );
        $report = Report::firstOrNew($params2);
        $report -> update($request -> all());
        $report->fill($request->all())->save();
        return redirect('/edit_daily_report')->with('status', '保存しました');
    }
}
