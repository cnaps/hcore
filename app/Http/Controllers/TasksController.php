<?php

namespace App\Http\Controllers;

use App\Member;
use App\Room;
use App\Task;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TasksController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    public function create()
    {
        $rooms = Room::all();
        $members = Member::all();
        return view('tasks.create', compact('rooms','members'));
    }

    public function store(Request $request)
    {
        Task::create($request->all());
        return redirect('/tasks');
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);
        return view('tasks.show', compact('task'));
    }

    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $rooms = Room::all();
        $members = Member::all();
        return view('tasks.edit', compact('task','rooms','members'));
    }

    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task -> update($request -> all());
        return redirect("/tasks");
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('/tasks');
    }

    public function storeTask(Request $request)
    {
        $place_id = Auth::user()->place_id;
        $date = date_format(Carbon::now(),'Y-m-d');
        $room_count = Room::where('place_id',$place_id)->count();

        for($i = 0;$i < $room_count;$i++) {
            if ($request->input('room_id'.$i) != null){
                $exist = Task::where('done_date',$date)->where('room_id', $request->input('room_id'.$i))->exists();
                if ($exist == false){
                    $task = new Task;
                    $task->room_id = $request->input('room_id'.$i);
                    $task->member_id = $request->input('member_id'.$i);
                    $task->done_date = $request->input('done_date');
                    $task->save();
                }else{
                    $task = Task::where('done_date',$date)->where('room_id', $request->input('room_id'.$i))->first();
                    $task->room_id = $request->input('room_id'.$i);
                    $task->member_id = $request->input('member_id'.$i);
                    $task->done_date = $request->input('done_date');
                    $task->save();
                }
            }
        }
        return redirect('/index_room');
    }

    public function storeOrderRoom(Request $request)
    {
        $place_id = Auth::user()->place_id;
        $today = date_format(Carbon::now(),'Y-m-d');
        $date = $request->input('done_date');
        $room_count = Room::where('place_id',$place_id)->count();

        for($i = 0;$i < $room_count;$i++) {
            if ($request->input('room_id'.$i) != null){
                $exist = Task::where('done_date',$date)->where('room_id', $request->input('room_id'.$i))->exists();
                if ($exist == false){
                    $task = new Task;
                    $task->room_id = $request->input('room_id'.$i);
                    $task->member_id = $request->input('member_id'.$i);
                    $task->order_flg = $request->input('order_flg'.$i);
                    $task->done_date = $request->input('done_date');
                    $task->save();
                }else{
                    $task = Task::where('done_date',$date)->where('room_id', $request->input('room_id'.$i))->first();
                    $task->room_id = $request->input('room_id'.$i);
                    $task->member_id = $request->input('member_id'.$i);
                    $task->order_flg = $request->input('order_flg'.$i);
                    $task->save();
                }
            }
        }
        if ($today == $date){
            return redirect('/create_order_room');
        }else{
            return redirect()->route('create_order_room2', [$date]);
        }
    }

    public function storeFinishRoom(Request $request)
    {
        $place_id = Auth::user()->place_id;
        $date = $request->input('done_date');
        $room_count = Room::where('place_id',$place_id)->count();

        for($i = 0;$i < $room_count;$i++) {
            if ($request->input('room_id'.$i) != null){
                $task = Task::where('done_date',$date)->where('room_id', $request->input('room_id'.$i))->first();
                $task->finish_flg = $request->input('finish_flg'.$i);
                $task->save();
            }
        }
        return redirect('/index_finish_room');
    }
}
