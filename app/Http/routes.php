<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


use App\Place;
Route::group(['middleware' => ['auth','admin']], function () {
    Route::get('/','RepliesController@indexNote');
    Route::resource('conditions', 'ConditionsController');
    Route::resource('daily_times', 'Daily_timesController');
    Route::resource('hotels', 'HotelsController');
    Route::resource('maintenance_rooms', 'Maintenance_roomsController');
    Route::resource('members', 'MembersController');
    Route::resource('notes', 'NotesController');
    Route::resource('places', 'PlacesController');
    Route::resource('replies', 'RepliesController');
    Route::resource('reports', 'ReportsController');
    Route::resource('report_details', 'Report_detailsController');
    Route::resource('rooms', 'RoomsController');
    Route::resource('room_types', 'Room_typesController');
    Route::resource('statuses', 'StatusesController');
    Route::resource('tasks', 'TasksController');
    Route::resource('times', 'TimesController');
    Route::resource('users', 'UsersController');

    //メール送信テスト用
    Route::get('/email_sending_test', function (){
        $data = [
            'title'=>'Infomation from H-Core',
            'content'=>'This is message below.'
        ];
        Mail::send('emails.test', $data, function($message){
            $message->to('t.morishita@takingshape.co','Tsutomu Morishita')->subject('Infomation');
        });
    });

    // タイムカード
    Route::get('time_card',[
        'as' => 'time_card',
        'uses' => 'TimesController@indexTimeCard'
    ]);
    Route::get('create_time_card/{member_id}',[
        'as' => 'create_time_card',
        'uses' => 'TimesController@createTimeCard'
    ]);
    Route::post('store_time_card',[
        'as' => 'store_time_card',
        'uses' => 'TimesController@storeTimeCard'
    ]);
    Route::get('create_pass/{member_id}',[
        'as' => 'create_pass',
        'uses' => 'TimesController@createPass'
    ]);
    Route::post('store_pass/{id}',[
        'as' => 'store_pass',
        'uses' => 'TimesController@storePass'
    ]);
    Route::get('change_pass/{member_id}',[
        'as' => 'change_pass',
        'uses' => 'TimesController@changePass'
    ]);
    Route::post('update_pass/{id}',[
        'as' => 'update_pass',
        'uses' => 'TimesController@updatePass'
    ]);
    Route::get('input_pass/{id}',[
        'as' => 'input_pass',
        'uses' => 'TimesController@inputPass'
    ]);
    Route::post('check_pass',[
        'as' => 'check_pass',
        'uses' => 'TimesController@checkPass'
    ]);
    //伝言掲示板
    Route::get('show_note/{id}',[
        'as' => 'show_note',
        'uses' => 'RepliesController@showNote'
    ]);
    Route::get('index_note',[
        'as' => 'index_note',
        'uses' => 'RepliesController@indexNote'
    ]);
    Route::get('create_note/{id}',[
        'as' => 'create_note',
        'uses' => 'RepliesController@createNote'
    ]);
    Route::post('store_note',[
        'as' => 'store_note',
        'uses' => 'RepliesController@storeNote'
    ]);
    Route::delete('delete_note/{id}',[
        'as' => 'delete_note',
        'uses' => 'RepliesController@deleteNote'
    ]);
    Route::get('create_master_note',[
        'as' => 'create_master_note',
        'uses' => 'NotesController@createMasterNote'
    ]);
    Route::post('store_master_note',[
        'as' => 'store_master_note',
        'uses' => 'NotesController@storeMasterNote'
    ]);
    Route::delete('delete_master_note/{id}',[
        'as' => 'delete_master_note',
        'uses' => 'NotesController@deleteMasterNote'
    ]);
    //部屋情報
    Route::get('index_room',[
        'as' => 'index_room',
        'uses' => 'RoomsController@indexRoom'
    ]);
    Route::post('store_task',[
        'as' => 'store_task',
        'uses' => 'TasksController@storeTask'
    ]);

//ホテル側部屋情報入力
    Route::get('create_order_room',[
        'as' => 'create_order_room',
        'uses' => 'RoomsController@createOrderRoom'
    ]);
    Route::get('create_order_room2/{date}',[
        'as' => 'create_order_room2',
        'uses' => 'RoomsController@createOrderRoom2'
    ]);
    Route::post('store_order_room',[
        'as' => 'store_order_room',
        'uses' => 'TasksController@storeOrderRoom'
    ]);
    Route::get('index_finish_room',[
        'as' => 'index_finish_room',
        'uses' => 'RoomsController@indexFinishRoom'
    ]);
    Route::post('store_finish_room',[
        'as' => 'store_finish_room',
        'uses' => 'TasksController@storeFinishRoom'
    ]);

//勤怠集計
    Route::get('index_all_times',[
        'as' => 'index_all_times',
        'uses' => 'Daily_timesController@indexAllTimes'
    ]);
    Route::post('index_all_times2',[
        'as' => 'index_all_times2',
        'uses' => 'Daily_timesController@indexAllTimes2'
    ]);

    Route::get('index_sales',[
        'as' => 'index_sales',
        'uses' => 'PlacesController@indexSales'
    ]);

//シフト登録
    Route::get('index_shift/{place_id}',[
        'as' => 'index_shift',
        'uses' => 'MembersController@indexShift'
    ]);

    Route::post('update_shift',[
        'as' => 'update_shift',
        'uses' => 'MembersController@updateShift'
    ]);

//地図表示
    Route::get('map',[
        'as' => 'map',
        'uses' => 'PlacesController@map'
    ]);

//会議用（一時的メニュー表示）
    Route::get('tmp_room_input',[
        'as' => 'tmp_room_input',
        'uses' => 'RoomsController@tmpRoomInput'
    ]);

    // 報告集計
    Route::get('index_daily_report',[
        'as' => 'index_daily_report',
        'uses' => 'ReportsController@indexDailyReport'
    ]);
    Route::post('index_daily_report2',[
        'as' => 'index_daily_report2',
        'uses' => 'ReportsController@indexDailyReport2'
    ]);

    //User パスワード変更
    Route::get('edit_pass/{id}',[
        'as' => 'edit_pass',
        'uses' => 'UsersController@editPass'
    ]);
    Route::put('update_pass/{id}',[
        'as' => 'update_pass',
        'uses' => 'UsersController@updatePass'
    ]);
});

    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    // Registration routes...
    Route::get('auth/register', 'Auth\AuthController@getRegister');
    Route::post('auth/register', 'Auth\AuthController@postRegister');

    // パスワードリセットリンクを要求するルート…
    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

    // パスワードリセットルート
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');

    //作業報告
    Route::get('create_daily_report',[
        'as' => 'create_daily_report',
        'uses' => 'ReportsController@createDailyReport'
    ]);
    Route::get('edit_daily_report',[
        'as' => 'edit_daily_report',
        'uses' => 'ReportsController@editDailyReport'
    ]);
    Route::post('check_daily_report',[
        'as' => 'check_daily_report',
        'uses' => 'ReportsController@checkDailyReport'
    ]);
    Route::post('store_daily_report',[
        'as' => 'store_daily_report',
        'uses' => 'ReportsController@storeDailyReport'
    ]);
    Route::put('update_daily_report',[
        'as' => 'update_daily_report',
        'uses' => 'ReportsController@updateDailyReport'
    ]);








