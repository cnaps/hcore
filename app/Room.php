<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    public function condition()
    {
        return $this->belongsTo('App\Condition');
    }
    public function room_type()
    {
        return $this->belongsTo('App\Room_type');
    }
    public function place()
    {
        return $this->belongsTo('App\Place');
    }
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function maintenance_rooms()
    {
        return $this->hasMany('App\Maintenance_room');
    }

    protected $fillable = [
        'floor',
        'room',
        'not_room',
        'condition_id',
        'place_id',
        'room_type_id',
        'v_num',
        'h_num',
        'v_span',
        'h_span',
    ];
}
