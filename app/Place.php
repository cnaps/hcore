<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends Model
{
    use SoftDeletes;

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }
    public function members()
    {
        return $this->hasMany('App\Member');
    }
    public function reports()
    {
        return $this->hasMany('App\Report');
    }
    public function notes()
    {
        return $this->hasMany('App\Note');
    }
    public function users()
    {
        return $this->hasMany('App\User');
    }

    protected $fillable = [
        'hotel_id',
        'name',
        'name_kana',
        'position',
        'address',
        'unit_price',
    ];
}
