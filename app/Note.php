<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;
    public function place()
    {
        return $this->belongsTo('App\Place');
    }
    public function member()
    {
        return $this->belongsTo('App\Member');
    }
    public function replies()
    {
        return $this->hasMany('App\Reply');
    }

    protected $fillable = [
        'title',
        'note',
        'member_id',
        'place_id',
    ];
}
