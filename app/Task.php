<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;
    public function room()
    {
        return $this->belongsTo('App\Room');
    }
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    protected $fillable = [
        'room_id',
        'member_id',
        'order_flg',
        'done_date',
        'finish_flg',
    ];
    protected $dates = [
        'done_date',
    ];
}
