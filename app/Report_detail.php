<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report_detail extends Model
{
//    use SoftDeletes;
    public function place()
    {
        return $this->belongsTo('App\Place');
    }
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    protected $fillable = [
        'place_id',
        'member_id',
        'date',
        'working_flg',
        'key_check',
        'make_total',
        'floor1',
        'room_count1',
        'floor2',
        'room_count2',
        'floor3',
        'room_count3',
        'floor4',
        'room_count4',
        'floor5',
        'room_count5',
        'other',
    ];
    protected $dates = [
        'date',
    ];
}
