<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reply extends Model
{
    use SoftDeletes;
    public function note()
    {
        return $this->belongsTo('App\Note');
    }
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    protected $fillable = [
        'note_id',
        'member_id',
        'note_reply',
    ];
}
