<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Time extends Model
{
    use SoftDeletes;
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    protected $fillable = [
        'member_id',
        'date',
        'time',
        'in_out',
    ];
}
