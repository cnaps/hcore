<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room_type extends Model
{
    use SoftDeletes;
    public function room()
    {
        return $this->hasMany('App\Room');
    }

    protected $fillable = [
        'name',
    ];
}
