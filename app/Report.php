<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
//    use SoftDeletes;
    public function place()
    {
        return $this->belongsTo('App\Place');
    }
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    protected $fillable = [
        'place_id',
        'member_id',
        'unit_price',
        'date',
        'unused_room_num',
        'note',
        'ordered_rooms_quantity',
        'cleaned_rooms_quantity',
    ];
    protected $dates = [
        'date',
    ];
}
