<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }

    protected $fillable = [
        'condition',
        'color',
    ];
}
