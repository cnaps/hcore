<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Model
{
    use SoftDeletes;
    public function status()
    {
        return $this->belongsTo('App\Status');
    }
    public function place()
    {
        return $this->belongsTo('App\Place');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function times()
    {
        return $this->hasMany('App\Time');
    }
    public function reports()
    {
        return $this->hasMany('App\Report');
    }
    public function notes()
    {
        return $this->hasMany('App\Note');
    }
    public function replies()
    {
        return $this->hasMany('App\Reply');
    }
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
    public function maintenance_rooms()
    {
        return $this->hasMany('App\Maintenance_room');
    }
    public function daily_times()
    {
        return $this->hasMany('App\Daily_time');
    }

    protected $fillable = [
        'last_name',
        'first_name',
        'last_name_kana',
        'first_name_kana',
        'tel',
        'tel_mp',
        'user_id',
        'postal_code',
        'pref',
        'address1',
        'address2',
        'building',
        'first_day',
        'final_day',
        'status_id',
        'place_id2',
        'base_salary',
        'traveling_expenses',
        'hourly_wage',
        'allowances',
        'paid_vacation',
        'pass',
        'working',
        'shift_monday',
        'shift_tuesday',
        'shift_wednesday',
        'shift_thursday',
        'shift_friday',
        'shift_saturday',
        'shift_sunday',
    ];
}
