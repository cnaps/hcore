@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/rooms/{{$room->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == $room->place_id)
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>階：</label>
            <input name="floor" value="{{$room->floor}}" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>部屋番号： （エレベーターの場合は"1"を入力してください。）</label>
            <input name="room" value="{{$room->room}}" type="number" class="form-control" placeholder="部屋番号を入力してください。">
        </div>
        <div class="form-group">
            <label>EV：</label>
            @if ($room->not_room == 1)
                <input name="not_room" type="hidden" value="0">
                <input name="not_room" type="checkbox" value="1" checked="checked">
            @else
                <input name="not_room" type="hidden" value="0">
                <input name="not_room" type="checkbox" value="1">
            @endif
        </div>
        <div class="form-group">
            <label>状態：</label>
            <select name="condition_id" class="form-control">
                @if($conditions)
                    @foreach($conditions as $condition)
                        <option value="{{$condition->id}}"
                                @if ($condition->id == old('condition_id'))
                                selected="selected"
                                @endif
                        >{{$condition->condition}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>ルームタイプ：</label>
            <select name="room_type_id" class="form-control">
                @if($room_types)
                    @foreach($room_types as $room_type)
                        <option value="{{$room_type->id}}"
                                @if ($room_type->id == old('room_type_id'))
                                selected="selected"
                                @endif
                        >{{$room_type->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>縦軸番号：</label>
            <input name="v_num" type="number" class="form-control" value="{{$room->v_num}}" placeholder="縦軸番号を入力してください。">
        </div>
        <div class="form-group">
            <label>横軸番号：</label>
            <input name="h_num" type="number" class="form-control" value="{{$room->h_num}}" placeholder="横軸番号を入力してください。">
        </div>
        <div class="form-group">
            <label>縦軸スパン：</label>
            <input name="v_span" type="number" class="form-control" value="{{$room->v_span}}" placeholder="縦軸スパンを入力してください。">
        </div>
        <div class="form-group">
            <label>横軸スパン：</label>
            <input name="h_span" type="number" class="form-control" value="{{$room->h_span}}" placeholder="横軸スパンを入力してください。">
        </div>
        <div class="pull-right">
            <a href="{{route('rooms.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('rooms.show', $room->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop