@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/rooms">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>階：</label>
            <input name="floor" type="number" class="form-control">
        </div>
        <hr>
        <ul>
            <li>登録したい部屋番号を10件まで一括で入力できます。</li>
            <li>部屋番号の入力がない行は登録されません。</li>
        </ul>
        <p>※ 各階のエレベーター位置も入力してください。以下のように入力できます。</p>
        <ul>
            <li>部屋番号：　1</li>
            <li>縦軸番号：　該当の番号</li>
            <li>横軸番号：　該当の番号</li>
        </ul>
        <div style="margin-top: 50px;">
            <div class="form-group">
                    <div class="row">
                        <div style="padding:5px !important" class="col-xs-1"></div>
                        <div style="padding:5px !important" class="col-xs-2">部屋番号</div>
                        <div style="padding:5px !important" class="col-xs-2">ルームタイプ</div>
                        <div style="padding:5px !important" class="col-xs-2">縦軸番号</div>
                        <div style="padding:5px !important" class="col-xs-2">横軸番号</div>
                        <div style="padding:5px !important" class="col-xs-2">縦軸スパン</div>
                        <div style="padding:5px !important" class="col-xs-2">横軸スパン</div>
                    </div>
                @for ($count = 1; $count < 41; $count++)
                    <div class="row">
                        <div style="padding:5px !important" class="col-xs-1">
                            <p style="text-align: right">{{$count}}. </p>
                        </div>
                        <div style="padding:5px !important" class="col-xs-2">
                            <input name="room_{{$count}}" type="number" class="form-control">
                        </div>

                            <input name="not_room_{{$count}}" type="hidden" value="0">

                        <div style="padding:5px !important" class="col-xs-2">
                            <select name="room_type_id_{{$count}}" class="form-control">
                                @if($room_types)
                                    @foreach($room_types as $room_type)
                                        <option value="{{$room_type->id}}">{{$room_type->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div style="padding:5px !important" class="col-xs-2">
                            <input name="v_num_{{$count}}" type="number" class="form-control">
                        </div>
                        <div style="padding:5px !important" class="col-xs-2">
                            <input name="h_num_{{$count}}" type="number" class="form-control">
                        </div>
                        <div style="padding:5px !important" class="col-xs-2">
                            <input name="v_span_{{$count}}" type="hidden" value="1">
                            <input name="v_span_{{$count}}" type="number" class="form-control">
                        </div>
                        <div style="padding:5px !important" class="col-xs-2">
                            <input name="h_span_{{$count}}" type="hidden" value="1">
                            <input name="h_span_{{$count}}" type="number" class="form-control">
                        </div>
                    </div>
                @endfor
            </div>
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('rooms.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop