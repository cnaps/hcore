@extends('layouts.app')
@section('content')
    <script type="text/javascript">
        $(function(){
            var count = 0;
            var order = 0;
            var member = 0;
            var finish = 0;
            var last = "";
            var first = "";
            $('.room').click(function () {
                count = $(this).data('count');
                last = $(this).data('last');
                first = $(this).data('first');
                order = $('input[name=order_flg'+ count +']').val();
                member = $('input[name=member_id'+ count +']').val();
                finish = $('input[name=finish_flg'+ count +']').val();
                if(order == 0 || member == 0){
                    return;
                }
                if(finish == 0){
                    $(this).css("background-color","#000000");
                    $('input[name=finish_flg'+ count +']').attr('value',"1");
                    $('div[name=last'+ count +']').text("完了");
                    $('div[name=first'+ count +']').text("");
                }else{
                    $(this).css("background-color","#0c9e16");
                    $('input[name=finish_flg'+ count +']').attr('value',"0");
                    $('div[name=last'+ count +']').text(last);
                    $('div[name=first'+ count +']').text(first);
                }
                count = 0;
            });
        });
    </script>
    <h1>清掃完了報告</h1>
    <form method="post" action="/store_finish_room">
        <input name="done_date" value="{{$today}}" type="hidden">
    <?php $name_count = 0; ?>
    {{--階--}}
    @for($i=$max_floor;$i > 0;$i--)
        @foreach($rooms as $room)
            @if($room->floor == $i)
                    <div style="font-weight:bold;background-color: #444444;color:#ffffff;border-radius: 0px;font-size: 35px;padding-top:10px;padding-bottom:10px;padding-left:20px;margin-bottom: 20px;border-bottom: 2px solid #999999">{{$room->floor}} F</div>
                    <?php break; ?>
            @endif
        @endforeach
        <div style="overflow-x: scroll;width: 100%;-webkit-overflow-scrolling: touch;">
            <table style="table-layout: fixed;border-spacing: 2px;border-collapse: separate;border:1px solid #ffffff;margin-top: 20px;" border="1">
        {{--縦軸番号--}}
        @for($v=1;$v<$max_v_num;$v++)
            <tr>
            {{--横軸番号--}}
            @for($h=1;$h<$max_h_num;$h++)
                <td style="width:110px;background-color: #EFEFEF;height: 110px;border-radius: 3px;">
                @if($rooms)
                    @foreach($rooms as $room)
                        @if($room->floor == $i)
                            @if($room->v_num == $v)
                                @if($room->h_num == $h)
                                    @if($room->room != 0)
                                        <?php $done = 0; ?>
                                        @foreach($tasks as $task)
                                                @if($room->id == $task->room_id)
                                                    @if($task->member_id == 0 && $task->order_flg == 1)
                                                        <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #bc2b39;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                            <div style="font-size: 30px;">{{$room->room}}</div>
                                                            <div name="last{{$name_count}}" style="font-size: 18px;"><a href="index_room" style="color: white;text-decoration: underline">担当者が設定されていません</a></div>
                                                            <div name="first{{$name_count}}" style="font-size: 12px;padding-top: 0px;"></div>
                                                        </div>
                                                        <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                        <input type="hidden" name="member_id{{$name_count}}" value="0">
                                                        <input type="hidden" name="order_flg{{$name_count}}" value="{{$task->order_flg}}">
                                                        <input type="hidden" name="finish_flg{{$name_count}}" value="{{$task->finish_flg}}">
                                                    @elseif($task->order_flg == 1)
                                                        @if($task->finish_flg == 0)
                                                            <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" data-last="{{$task->member->last_name}}" data-first="{{$task->member->first_name}}" style="width:110px;height:100%;background-color: #0c9e16;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                                <div style="font-size: 30px;">{{$room->room}}</div>
                                                                <div name="last{{$name_count}}" style="font-size: 20px;">{{$task->member->last_name}}</div>
                                                                <div name="first{{$name_count}}" style="font-size: 14px;padding-top: 0px;">{{$task->member->first_name}}</div>
                                                            </div>
                                                            <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                            <input type="hidden" name="member_id{{$name_count}}" value="{{$task->member_id}}">
                                                            <input type="hidden" name="order_flg{{$name_count}}" value="{{$task->order_flg}}">
                                                            <input type="hidden" name="finish_flg{{$name_count}}" value="{{$task->finish_flg}}">
                                                        @else
                                                            <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" data-last="{{$task->member->last_name}}" data-first="{{$task->member->first_name}}" style="width:110px;height:100%;background-color: #000000;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                                <div style="font-size: 30px;">{{$room->room}}</div>
                                                                <div name="last{{$name_count}}" style="font-size: 20px;">完了</div>
                                                                <div name="first{{$name_count}}" style="font-size: 14px;padding-top: 0px;"></div>
                                                            </div>
                                                            <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                            <input type="hidden" name="member_id{{$name_count}}" value="{{$task->member_id}}">
                                                            <input type="hidden" name="order_flg{{$name_count}}" value="{{$task->order_flg}}">
                                                            <input type="hidden" name="finish_flg{{$name_count}}" value="{{$task->finish_flg}}">
                                                        @endif
                                                    @else
                                                        <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #aaaaaa;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                            <div style="font-size: 30px;">{{$room->room}}</div>
                                                            <div name="last{{$name_count}}" style="font-size: 20px;"></div>
                                                            <div name="first{{$name_count}}" style="font-size: 12px;padding-top: 0px;"></div>
                                                        </div>
                                                        <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                        <input type="hidden" name="member_id{{$name_count}}" value="0">
                                                        <input type="hidden" name="order_flg{{$name_count}}" value="0">
                                                        <input type="hidden" name="finish_flg{{$name_count}}" value="{{$task->finish_flg}}">
                                                    @endif
                                                @endif
                                        @endforeach
                                        <?php $name_count++; ?>
                                    @else
                                        <button type="button" style="width:100%;height:100%;background-color: #444444;margin:0px;border-radius: 3px;padding:5px;font-size: 20px;color:#fff;text-align: center;">E V</button>
                                    @endif
                                @endif
                            @endif
                        @endif
                    @endforeach
                @endif
                </td>
            @endfor
             </tr>
        @endfor
        </table>
        </div>
        <button type="submit" class="pull-right btn btn-primary btn-lg" style="margin-top:20px;margin-bottom:150px;">登録</button>
        <div class="clearfix"></div>
    @endfor
        {{csrf_field()}}
    </form>
@stop


