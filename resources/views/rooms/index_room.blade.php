@extends('layouts.app')
@section('content')
    <script type="text/javascript">
        $(function(){
            var keep = [];
            var count = 0;
            var memberId = 0;
            var last = "";
            var first = "";
            var sameFlg = 0;
            var keepIndex = 0;
            var order;
            $('.room').click(function () {
                count = $(this).data('count');
                order = $('input[name=order_flg'+ count +']').val();
                if(order == 0){
                    return;
                }
                $.each(keep,
                    function (i, value) {
                        if(count === value){
                            keepIndex = i;
                            sameFlg = 1;
                        }
                    });
                if(sameFlg === 0){
                    keep.push(count);
                    console.log(keep);
                    $(this).css("background-color","#e01d54");
                }else{
                    keep.splice(keepIndex,1);
                    console.log(keep);
                    $(this).css("background-color","#337ab7");
                }
                count = 0;
                sameFlg = 0;
                keepIndex = 0;
            });
            $('.member').click(function () {
                memberId = $(this).data('memberid');
                if(memberId == 0){
                    $.each(keep,
                        function (i, value) {
                            $('input[name=member_id'+ value +']').attr('value',memberId);
                            $('div[name=member'+ value +']').text("0");
                            $('div[name=last'+ value +']').text("");
                            $('div[name=first'+ value +']').text("");
                            $('div[name=roombutton'+ value +']').css("background-color","#337ab7");
                        });
                    keep.length = 0;
                }else{
                    last = $(this).data('last');
                    first = $(this).data('first');
                    $.each(keep,
                        function (i, value) {
                            $('input[name=member_id'+ value +']').attr('value',memberId);
                            $('div[name=last'+ value +']').text(last);
                            $('div[name=first'+ value +']').text(first);
                            $('div[name=roombutton'+ value +']').css("background-color","#0c9e16");
                        });
                    keep.length = 0;
                }
            })
        });
    </script>
    <h1>清掃担当入力</h1>
    <form method="post" action="/store_task">
    <?php $name_count = 0; ?>
    {{--階--}}
    @for($i=$max_floor;$i > 0;$i--)
        @foreach($rooms as $room)
            @if($room->floor == $i)
                    <div style="font-weight:bold;background-color: #444444;color:#ffffff;border-radius: 0px;font-size: 35px;padding-top:10px;padding-bottom:10px;padding-left:20px;margin-bottom: 20px;border-bottom: 2px solid #999999">{{$room->floor}} F</div>
                    <button type="button" data-memberid="0" class="btn btn-danger btn-lg member" style="margin:5px;font-size: 14pt;padding:10px">クリア</button>
                    @foreach($members as $member)
                        <button type="button" data-count="0" data-memberid="{{$member->id}}" data-last="{{$member->last_name}}" data-first="{{$member->first_name}}" class="btn btn-default btn-lg member" style="margin:5px;font-size: 14pt;padding: 10px">{{$member->last_name}} {{$member->first_name}}</button>
                    @endforeach
                    <?php break; ?>
            @endif
        @endforeach
        <div style="overflow-x: scroll;width: 100%;-webkit-overflow-scrolling: touch;">
            <table style="table-layout: fixed;border-spacing: 2px;border-collapse: separate;border:1px solid #ffffff;margin-top: 20px;" border="1">
            {{--縦軸番号--}}
            @for($v=1;$v<$max_v_num;$v++)
                <tr>
                {{--横軸番号--}}
                @for($h=1;$h<$max_h_num;$h++)
                    <td style="width:110px;background-color: #EFEFEF;height: 110px;border-radius: 3px;">
                    @if($rooms)
                        @foreach($rooms as $room)
                            @if($room->floor == $i)
                                @if($room->v_num == $v)
                                    @if($room->h_num == $h)
                                        @if($room->room != 0)
                                            <?php $done = 0; ?>
                                            @foreach($tasks as $task)
                                                @if($room->id == $task->room_id)
                                                    @if($task->member_id == 0 && $task->order_flg == 1)
                                                            <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #337ab7;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                                <div style="font-size: 30px;">{{$room->room}}</div>
                                                                <div name="last{{$name_count}}" style="font-size: 20px;"></div>
                                                                <div name="first{{$name_count}}" style="font-size: 12px;padding-top: 0px;"></div>
                                                            </div>
                                                            <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                            <input type="hidden" name="member_id{{$name_count}}" value="0">
                                                            <input type="hidden" name="order_flg{{$name_count}}" value="{{$task->order_flg}}">
                                                    @elseif($task->order_flg == 1)
                                                        <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #0c9e16;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                            <div style="font-size: 30px;">{{$room->room}}</div>
                                                            <div name="last{{$name_count}}" style="font-size: 20px;">{{$task->member->last_name}}</div>
                                                            <div name="first{{$name_count}}" style="font-size: 14px;padding-top: 0px;"> {{$task->member->first_name}} </div>
                                                        </div>
                                                        <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                        <input type="hidden" name="member_id{{$name_count}}" value="{{$task->member_id}}">
                                                        <input type="hidden" name="order_flg{{$name_count}}" value="{{$task->order_flg}}">
                                                    @else
                                                        <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #aaaaaa;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                            <div style="font-size: 30px;">{{$room->room}}</div>
                                                            <div name="last{{$name_count}}" style="font-size: 20px;"></div>
                                                            <div name="first{{$name_count}}" style="font-size: 12px;padding-top: 0px;"></div>
                                                        </div>
                                                        <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                        <input type="hidden" name="member_id{{$name_count}}" value="0">
                                                        <input type="hidden" name="order_flg{{$name_count}}" value="0">
                                                    @endif
                                                @endif
                                            @endforeach
                                            <?php $name_count++; ?>
                                        @else
                                            <button type="button" style="width:100%;height:100%;background-color: #444444;margin:0px;border-radius: 3px;padding:5px;font-size: 20px;color:#fff;text-align: center;">E V</button>
                                        @endif
                                    @endif
                                @endif
                            @endif
                        @endforeach
                    @endif
                    </td>
                @endfor
                 </tr>
            @endfor
            </table>
        </div>
        <button type="submit" class="pull-right btn btn-primary btn-lg" style="margin-top:20px;margin-bottom:150px;">登録</button>
        <div class="clearfix"></div>
    @endfor
        <input type="hidden" name="done_date" value="<?php use Carbon\Carbon;echo Carbon::now()->format('Y-m-d') ?>">
        {{csrf_field()}}
    </form>
@stop


