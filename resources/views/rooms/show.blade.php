@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>部屋ID</th>
                <td>{{$room->id}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$room->place->name}}</td>
            </tr>
            <tr>
                <th>部屋番号</th>
                <td>{{$room->room}}</td>
            </tr>
            <tr>
                <th>状態</th>
                <td>{{$room->condition->condition}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$room->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$room->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('rooms.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('rooms.edit', $room->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop