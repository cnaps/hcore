@extends('layouts.app')
@section('content')
    <script type="text/javascript">
        $(function(){
            var count = 0;
            var val = 0;
            $('input[name=done_date]').change(function() {
                val = $('input[name=done_date]').val();
                window.location.href = "/create_order_room2/" + val;
            });
            $('.room').click(function () {
                count = $(this).data('count');
                val = $('input[name=order_flg'+ count +']').val();
                if(val == 0){
                    $(this).css("background-color","#e01d54");
                    $('input[name=order_flg'+ count +']').attr('value',"1");
                }else{
                    $(this).css("background-color","#337ab7");
                    $('input[name=order_flg'+ count +']').attr('value',"0");
                }
                count = 0;
            });
        });
    </script>
    <h1>清掃指示入力</h1>
    <form method="post" action="/store_order_room">
        <div class="form-group">
            <label>指示日：</label>
            <input name="done_date" value="{{$date}}" type="date" class="form-control">
        </div>
    <?php $name_count = 0; ?>
    {{--階--}}
    @for($i=$max_floor;$i > 0;$i--)
        @foreach($rooms as $room)
            @if($room->floor == $i)
                    <div style="font-weight:bold;background-color: #444444;color:#ffffff;border-radius: 0px;font-size: 35px;padding-top:10px;padding-bottom:10px;padding-left:20px;margin-bottom: 20px;border-bottom: 2px solid #999999">{{$room->floor}} F</div>
                    <?php break; ?>
            @endif
        @endforeach
        <div style="overflow-x: scroll;width: 100%;-webkit-overflow-scrolling: touch;">
            <table style="table-layout: fixed;border-spacing: 2px;border-collapse: separate;border:1px solid #ffffff;margin-top: 20px;" border="1">
        {{--縦軸番号--}}
        @for($v=1;$v<$max_v_num;$v++)
            <tr>
            {{--横軸番号--}}
            @for($h=1;$h<$max_h_num;$h++)
                <td style="width:110px;background-color: #EFEFEF;height: 110px;border-radius: 3px;">
                @if($rooms)
                    @foreach($rooms as $room)
                        @if($room->floor == $i)
                            @if($room->v_num == $v)
                                @if($room->h_num == $h)
                                    @if($room->room != 0)
                                        <?php $done = 0; ?>
                                        @foreach($tasks as $task)
                                            @if($room->id == $task->room_id && $task->order_flg == 1)
                                                    <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #e01d54;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                        <div style="font-size: 30px;">{{$room->room}}</div>
                                                    </div>
                                                    <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                                    <input type="hidden" name="member_id{{$name_count}}" value="{{$task->member_id}}">
                                                    <input type="hidden" name="order_flg{{$name_count}}" value="{{$task->order_flg}}">
                                                    <?php $done = 1; ?>
                                            @endif
                                            @if($done == 1)
                                                <?php break; ?>
                                            @endif
                                        @endforeach
                                        @if($done == 0)
                                            <div type="button" class="room" name="roombutton{{$name_count}}" data-count="{{$name_count}}" style="width:110px;height:100%;background-color: #337ab7;margin:0px;border-radius: 3px;font-size: 20px;color:#fff;text-align: center;">
                                                <div style="font-size: 30px;">{{$room->room}}</div>
                                            </div>
                                            <input type="hidden" name="room_id{{$name_count}}" value="{{$room->id}}">
                                            <input type="hidden" name="member_id{{$name_count}}" value="0">
                                            <input type="hidden" name="order_flg{{$name_count}}" value="0">
                                        @endif
                                        <?php $name_count++; ?>
                                    @else
                                        <button type="button" style="width:100%;height:100%;background-color: #444444;margin:0px;border-radius: 3px;padding:5px;font-size: 20px;color:#fff;text-align: center;">E V</button>
                                    @endif
                                @endif
                            @endif
                        @endif
                    @endforeach
                @endif
                </td>
            @endfor
             </tr>
        @endfor
        </table>
        </div>
        <button type="submit" class="pull-right btn btn-primary btn-lg" style="margin-top:20px;margin-bottom:150px;">登録</button>
        <div class="clearfix"></div>
    @endfor
        {{csrf_field()}}
    </form>
@stop


