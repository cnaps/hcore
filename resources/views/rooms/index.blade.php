@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('rooms.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>部屋ID</th>
                <th>物件名</th>
                <th>階</th>
                <th>部屋番号</th>
                <th>EV</th>
                <th>ルームタイプ</th>
                {{--<th>状態</th>--}}
                <th>縦軸番号</th>
                <th>横軸番号</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($rooms)
            @foreach($rooms as $room)

                <tr>
                    <td>{{$room->id}}</td>
                    <td>{{$room->place->name}}</td>
                    <td>{{$room->floor}}</td>
                    <td>{{$room->room}}</td>
                    @if($room->not_room == 1)
                        <td>はい</td>
                    @else
                        <td>いいえ</td>
                    @endif
                    <td>{{$room->room_type->name}}</td>
                    {{--<td>{{$room->condition->condition}}</td>--}}
                    <td>{{$room->v_num}}</td>
                    <td>{{$room->h_num}}</td>
                    <td>{{$room->updated_at->diffForhumans()}}</td>
                    <td>{{$room->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('rooms.show', $room->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('rooms.edit', $room->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop