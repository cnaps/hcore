<!doctype html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <!-- InternetExplorerのブラウザではバージョンによって崩れることがあるので、互換表示をさせないために設定するmetaタグです。 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- レスポンシブWebデザインを使うために必要なmetaタグです。 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="//unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <link rel="apple-touch-icon" sizes="180x180" href="./img/apple-touch-icon.png">
    <!--<link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32x32.png">-->
    <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16x16.png">
    <link rel="manifest" href="./img/manifest.json">
    <link rel="mask-icon" href="./img/safari-pinned-tab.svg" color="#276196">
    <link rel="shortcut icon" href="./img/favicon.ico">
    <meta name="msapplication-config" content="./img/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <title>PS CORE - Pioneer System Inc</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/tableexport.min.css">
    <style>
        @import url(https://fonts.googleapis.com/earlyaccess/notosansjapanese.css);

        body {
            padding-top: 50px;
            font-family: 'Noto Sans Japanese';
            /*font-weight: 200;*/
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        a {
            text-decoration: none;
        }
        td{
            vertical-align: middle !important;
        }
        button{
            background-color: transparent;
            border: none;
            cursor: pointer;
            outline: none;
            padding: 0;
            appearance: none;
        }
    </style>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="//unpkg.com/leaflet@1.2.0/dist/leaflet.js"
            integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
            crossorigin=""></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script language="JavaScript">
        <!--
        function set2fig(num) {
            // 桁数が1桁だったら先頭に0を加えて2桁に調整する
            var ret;
            if( num < 10 ) { ret = "0" + num; }
            else { ret = num; }
            return ret;
        }
        function showClock2() {
            var nowTime = new Date();
            var nowHour = set2fig( nowTime.getHours() );
            var nowMin  = set2fig( nowTime.getMinutes() );
            var nowSec  = set2fig( nowTime.getSeconds() );
            document.getElementById("RealtimeClockArea2").innerHTML = "現在時刻：" + nowHour + " 時 " + nowMin + " 分 " + nowSec + " 秒";
        }
        setInterval('showClock2()',1000);
        // -->
    </script>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @if (Auth::guest())
                <div style="padding-top:8px;font-weight:bold;font-size: 25px;height:50px;color:#fff;">PS CORE</div>
            @else
                <div style="font-size: 25px;height:50px;"><img style="width: 200px" src="img/logo.png"/></div>
            @endif

        </div>
        @yield('nav')
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <!--<li style="padding-left: 20px;"><a href="/auth/login">ログイン</a></li>-->
                @else
                    <?php $user = Auth::user(); ?>
                    @if($user->authority_id == 3)
                        <li style="padding-left: 20px;"><a href="/index_note">伝言ノート</a></li>
                        <li><a href="/time_card">タイムカード</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">管理 <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "/index_all_times" ?>">勤怠集計</a></li>
                            </ul>
                        </li>
                    @elseif($user->authority_id == 2)
                        <li style="padding-left: 20px;"><a href="/index_note">伝言ノート</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">フロント <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "/create_order_room" ?>">清掃部屋指示</a></li>
                            </ul>
                        </li>
                    @elseif($user->authority_id == 4)
<!--                        <li><a href="/time_card">タイムカード</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">報告 <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "/create_daily_report" ?>">作業報告</a></li>
                                <li><a href="<?php echo "/index_room" ?>">清掃担当割り振り</a></li>
                                <li><a href="<?php echo "/index_finish_room" ?>">清掃完了報告</a></li>
                            </ul>
                        </li>-->
                    @elseif($user->authority_id == 1)
                        <li style="padding-left: 20px;"><a href="/index_note">伝言ノート</a></li>
                        <li><a href="/time_card">タイムカード</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">フロント <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "/create_order_room" ?>">清掃部屋指示</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">報告 <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "/create_daily_report" ?>">作業報告</a></li>
                                <li><a href="<?php echo "/index_room" ?>">清掃担当割り振り</a></li>
                                <li><a href="<?php echo "/index_finish_room" ?>">清掃完了報告</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">管理 <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo "/index_sales" ?>">売上情報</a></li>
                                <li><a href="<?php echo "/map" ?>" target="_blank">稼働率マップ</a></li>
                                <li><a href="<?php echo "/index_all_times" ?>">勤怠集計</a></li>
                                <li><a href="<?php echo "/index_shift/1" ?>">シフト登録</a></li>
                                <li><a href="<?php echo "/index_daily_report" ?>">業務報告集計表</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">マスタ <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="dropdown-header">マスタテーブル管理</li>
                                <li><a href="<?php echo "/hotels" ?>">ホテル</a></li>
                                <li><a href="<?php echo "/places" ?>">物件</a></li>
                                <li><a href="<?php echo "/conditions" ?>">状態</a></li>
                                <li><a href="<?php echo "/members" ?>">従業員</a></li>
                                <li><a href="<?php echo "/rooms" ?>">部屋番号</a></li>
                                <li><a href="<?php echo "/room_types" ?>">ルームタイプ</a></li>
                                <li><a href="<?php echo "/statuses" ?>">勤務形態</a></li>
                                <li><a href="<?php echo "/users" ?>">アカウント一覧</a></li>
                                <li><a href="/users/create">アカウント作成</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-header">データテーブル管理</li>
                                <li><a href="<?php echo "/maintenance_rooms" ?>">修理部屋</a></li>
                                <li><a href="<?php echo "/notes" ?>">備考</a></li>
                                <li><a href="<?php echo "/replies" ?>">備考返信</a></li>
                                <li><a href="<?php echo "/reports" ?>">報告</a></li>
                                <li><a href="<?php echo "/report_details" ?>">報告詳細</a></li>
                                <li><a href="<?php echo "/tasks" ?>">タスク</a></li>
                                <li><a href="<?php echo "/times" ?>">タイムカード</a></li>
                                <li><a href="<?php echo "/daily_times" ?>">時間記録</a></li>
                            </ul>
                        </li>
                    @endif
                    <!-- ドロップダウンメニュー -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }}
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/auth/logout">ログアウト</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>

<div class="container" style="padding-top: 50px;padding-bottom: 100px;">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

        @yield('content')

</div>
<!-- /.container -->
<!-- BootstrapのJS読み込み -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/xlsx.core.min.js"></script>
<script type="text/javascript" src="./js/FileSaver.min.js"></script>
<script type="text/javascript" src="./js/tableexport.min.js"></script>
<script type="text/javascript" src="./js/test.js"></script>
</body>

</html>