<!doctype html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <!-- InternetExplorerのブラウザではバージョンによって崩れることがあるので、互換表示をさせないために設定するmetaタグです。 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- レスポンシブWebデザインを使うために必要なmetaタグです。 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="//unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <link rel="apple-touch-icon" sizes="180x180" href="./img/apple-touch-icon.png">
    <!--<link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32x32.png">-->
    <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16x16.png">
    <link rel="manifest" href="./img/manifest.json">
    <link rel="mask-icon" href="./img/safari-pinned-tab.svg" color="#276196">
    <link rel="shortcut icon" href="./img/favicon.ico">
    <meta name="msapplication-config" content="./img/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <title>PS CORE - Pioneer System Inc</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/tableexport.min.css">
    <style>
        @import url(https://fonts.googleapis.com/earlyaccess/notosansjapanese.css);

        body {
            font-family: 'Noto Sans Japanese';
            /*font-weight: 200;*/
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        a {
            text-decoration: none;
        }
        td{
            vertical-align: middle !important;
        }
        button{
            background-color: transparent;
            border: none;
            cursor: pointer;
            outline: none;
            padding: 0;
            appearance: none;
        }
    </style>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="//unpkg.com/leaflet@1.2.0/dist/leaflet.js"
            integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
            crossorigin=""></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script language="JavaScript">
        <!--
        function set2fig(num) {
            // 桁数が1桁だったら先頭に0を加えて2桁に調整する
            var ret;
            if( num < 10 ) { ret = "0" + num; }
            else { ret = num; }
            return ret;
        }
        function showClock2() {
            var nowTime = new Date();
            var nowHour = set2fig( nowTime.getHours() );
            var nowMin  = set2fig( nowTime.getMinutes() );
            var nowSec  = set2fig( nowTime.getSeconds() );
            document.getElementById("RealtimeClockArea2").innerHTML = "現在時刻：" + nowHour + " 時 " + nowMin + " 分 " + nowSec + " 秒";
        }
        setInterval('showClock2()',1000);
        // -->
    </script>
</head>

<body>
<div class="container" style="padding-top:50px;padding-bottom: 100px;">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
        @if (Auth::guest())
        @else
            <div style="text-align: right;margin-top: 20px;"><a class="btn btn-primary" href="/auth/logout">ログアウト</a></div>
        @endif
        @yield('content')

</div>
<!-- /.container -->
<!-- BootstrapのJS読み込み -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./js/xlsx.core.min.js"></script>
<script type="text/javascript" src="./js/FileSaver.min.js"></script>
<script type="text/javascript" src="./js/tableexport.min.js"></script>
<script type="text/javascript" src="./js/test.js"></script>
</body>

</html>