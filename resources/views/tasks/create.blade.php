@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/tasks">
        <div class="form-group">
            <label>部屋番号：</label>
            <select name="room_id" class="form-control">
                @if($rooms)
                    @foreach($rooms as $room)
                        <option value="{{$room->id}}">{{$room->place->name}} {{$room->room}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>担当者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>実施日：</label>
            <input name="done_date" value="<?php use Carbon\Carbon;echo Carbon::now()->format('Y-m-d') ?>" type="date" class="form-control">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('tasks.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop