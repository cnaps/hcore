@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/tasks/{{$task->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>部屋番号：</label>
            <select name="room_id" class="form-control">
                @if($rooms)
                    @foreach($rooms as $room)
                        <option value="{{$room->id}}"
                                @if ($room->id == old('room_id'))
                                selected="selected"
                                @endif
                        >{{$room->place->name}} {{$room->room}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>担当者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == old('member_id'))
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>実施日：</label>
            <input name="done_date" value="{{$task->done_date->format('Y-m-d')}}" type="date" class="form-control">
        </div>
        <div class="pull-right">
            <a href="{{route('tasks.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('tasks.show', $task->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop