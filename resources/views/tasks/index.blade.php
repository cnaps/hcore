@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('tasks.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>タスクID</th>
                <th>物件名</th>
                <th>部屋番号</th>
                <th>担当者</th>
                <th>実施日</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($tasks)
            @foreach($tasks as $task)

                <tr>
                    <td>{{$task->id}}</td>
                    <td>{{$task->room->place->name}}</td>
                    <td>{{$task->room->room}}</td>
                    @if($task->member_id != 0 || $task->member_id != null)
                        <td>{{$task->member->last_name}} {{$task->member->last_name}}</td>
                    @else
                        <td></td>
                    @endif
                    <td>{{$task->done_date->format('Y-m-d')}}</td>
                    <td>{{$task->updated_at->diffForhumans()}}</td>
                    <td>{{$task->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('tasks.show', $task->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop