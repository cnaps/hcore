@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>タスクID</th>
                <td>{{$task->id}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$task->room->place->name}}</td>
            </tr>
            <tr>
                <th>部屋番号</th>
                <td>{{$task->room->room}}</td>
            </tr>
            <tr>
                <th>担当者</th>
                <td>{{$task->member->last_name}} {{$task->member->first_name}}</td>
            </tr>
            <tr>
                <th>実施日</th>
                <td>{{$task->done_date->format('Y-m-d')}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$task->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$task->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('tasks.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('tasks.edit', $task->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop