@extends('layouts.app')
@section('content')
    <h1>勤怠集計表</h1>
    <form method="post" action="{{route('index_all_times2')}}">
        <div class="container-fluid">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="form-group">
                        <label>ホテル：</label>
                        <select name="place" class="form-control">
                            @if($places)
                                @foreach($places as $place)
                                    @if($place_id == $place->id)
                                        <option value="{{$place->id}}" selected="selected">{{$place->name}}</option>
                                    @else
                                        <option value="{{$place->id}}">{{$place->name}}</option>
                                    @endif
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>月選択：</label>
                        <input name="month" type="month" value="{{$month}}" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>クリックで更新</label>
                        <button name="submit" type="submit" class="form-control">変更</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <table id="DailyTimes" class="table table-striped">
        <thead>
            <tr>
                <th>従業員ID</th>
                <th>該当者</th>
                <th class="text-right">時間</th>
                <th class="text-right">分</th>
                <th class="text-right">時給</th>
                <th class="text-right">交通費片道単価</th>
                <th class="text-right">勤務日数</th>
                <th class="text-right">交通費合計</th>
                <th class="text-right">給与総額</th>
            </tr>
        </thead>
        <tbody>
        @if($members)
            @foreach($members as $member)
                <tr>
                @foreach($day_counts as $day_count)
                        <?php
                        $done = 0;
                        $days = 0;
                        $hour = 0;
                        $min = 0;
                        $min_wage = 0;
                        $travel = 0;
                        $price = 0;
                        $hwage = 0;
                        $unit_travel = 0;
                        $hwage = 0;
                        $unit_travel = 0;
                        ?>
                    @if($day_count->member_id == $member->id)
                        <?php $days = $day_count->date_count; ?>
                        <?php break; ?>
                    @endif
                @endforeach
                @if($daily_times)
                    @foreach($daily_times as $daily_time)
                        @if($daily_time->member_id == $member->id)
                            <?php
                                $hour = floor($daily_time->total_time / 60);
                                $min = $daily_time->total_time - ($hour * 60);
                                $min_wage = $member->hourly_wage / 60;
                                $travel = $member->traveling_expenses * $days * 2;
                                $price = floor($hour * $member->hourly_wage + $min_wage * $min + $travel);
                                $hwage = $member->hourly_wage;
                                $unit_travel = $member->traveling_expenses;
                                ?>
                            <td>{{$member->id}}</td>
                            <td>{{$member->last_name}} {{$member->first_name}}</td>
                            <td class="text-right">{{$hour}}</td><td class="text-right">{{$min}}</td>
                            <td class="text-right">{{$hwage}}</td>
                            <td class="text-right">{{$unit_travel}}</td>
                            <td class="text-right">{{$days}}</td>
                            <td class="text-right">{{$travel}}</td>
                            <td class="text-right">{{$price}}</td>
                            <?php $done = 1; ?>
                        @endif
                    @endforeach
                    @if($done == 0)
                        <td>{{$member->id}}</td>
                        <td>{{$member->last_name}} {{$member->first_name}}</td>
                        <td class="text-right">0</td>
                        <td class="text-right">0</td>
                        <td class="text-right">0</td>
                        <td class="text-right">0</td>
                        <td class="text-right">0</td>
                        <td class="text-right">0</td>
                        <td class="text-right">0</td>
                    @endif
                @else
                    <td>{{$member->id}}</td>
                    <td>{{$member->last_name}} {{$member->first_name}}</td>
                    <td class="text-right">0</td>
                    <td class="text-right">0</td>
                    <td class="text-right">0</td>
                    <td class="text-right">0</td>
                    <td class="text-right">0</td>
                    <td class="text-right">0</td>
                    <td class="text-right">0</td>
                @endif
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop