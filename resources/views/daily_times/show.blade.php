@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>記録ID</th>
                <td>{{$daily_time->id}}</td>
            </tr>
            <tr>
                <th>該当者</th>
                <td>{{$daily_time->member->last_name}} {{$daily_time->member->first_name}}</td>
            </tr>
            <tr>
                <th>時間（分）</th>
                <td>{{$daily_time->time}}</td>
            </tr>
            <tr>
                <th>日付</th>
                <td>{{$daily_time->date}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$daily_time->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$daily_time->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('daily_times.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('daily_times.edit', $daily_time->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop