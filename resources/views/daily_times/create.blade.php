@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/daily_times">
        <div class="form-group">
            <label>該当者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>時間：</label>
            <input name="time" type="number" class="form-control" placeholder="時間を分単位で入力してください。">
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" type="date" placeholder="日付を入力してください。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('daily_times.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop