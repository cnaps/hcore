@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('daily_times.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>記録ID</th>
                <th>該当者</th>
                <th>時間（分）</th>
                <th>日付</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($daily_times)
            @foreach($daily_times as $daily_time)

                <tr>
                    <td>{{$daily_time->id}}</td>
                    <td>{{$daily_time->member->last_name}} {{$daily_time->member->first_name}}</td>
                    <td>{{$daily_time->time}}</td>
                    <td>{{$daily_time->date}}</td>
                    <td>{{$daily_time->updated_at->diffForhumans()}}</td>
                    <td>{{$daily_time->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('daily_times.show', $daily_time->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('daily_times.edit', $daily_time->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop