@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/notes">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>タイトル：</label>
            <input name="title" type="text" class="form-control" placeholder="内容を簡潔に要約して入力してください。">
        </div>
        <div class="form-group">
            <label>伝達事項：</label>
            <textarea name="note" placeholder="伝達事項の詳細を入力してください。" rows="4" class="form-control" id="InputTextarea"></textarea>
        </div>
        <div class="form-group">
            <label>投稿者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('notes.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop