@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('notes.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>投稿ID</th>
                <th>タイトル</th>
                <th>物件名</th>
                <th>投稿内容</th>
                <th>投稿者</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($notes)
            @foreach($notes as $note)

                <tr>
                    <td>{{$note->id}}</td>
                    <td>{{$note->title}}</td>
                    <td>{{$note->place->name}}</td>
                    <td>{{$note->note}}</td>
                    <td>{{$note->member->last_name}} {{$note->member->last_name}}</td>
                    <td>{{$note->updated_at->diffForhumans()}}</td>
                    <td>{{$note->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('notes.show', $note->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('notes.edit', $note->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop