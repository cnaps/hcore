@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/notes/{{$note->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == old('place_id'))
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>タイトル：</label>
            <input name="title" value="{{$note->title}}" type="text" class="form-control" placeholder="内容を簡潔に要約して入力してください。">
        </div>
        <div class="form-group">
            <label>伝言内容：</label>
            <textarea name="note" placeholder="修理が必要な箇所に関して詳細を記述してください。" rows="4" class="form-control" id="InputTextarea">{{$note->note}}</textarea>
        </div>
        <div class="form-group">
            <label>投稿者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == old('member_id'))
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="pull-right">
            <a href="{{route('notes.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('notes.show', $note->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop