@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('room_types.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ルームタイプID</th>
                <th>ルームタイプ</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($room_types)
            @foreach($room_types as $room_type)
                <tr>
                    <td>{{$room_type->id}}</td>
                    <td>{{$room_type->name}}</td>
                    <td>{{$room_type->updated_at->diffForhumans()}}</td>
                    <td>{{$room_type->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('room_types.show', $room_type->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('room_types.edit', $room_type->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop