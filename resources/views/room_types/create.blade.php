@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/room_types">
        <div class="form-group">
            <label>ルームタイプ名：</label>
            <input name="name" type="text" class="form-control" placeholder="ルームタイプ名を入力して下さい。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('room_types.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop