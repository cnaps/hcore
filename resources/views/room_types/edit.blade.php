@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/room_types/{{$room_types->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>ルームタイプ名：</label>
            <input name="name" type="text" class="form-control" value="{{$room_type->name}}" placeholder="ルームタイプ名を入力して下さい。">
        </div>
        <div class="pull-right">
            <a href="{{route('room_types.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('room_types.show', $room_type->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop