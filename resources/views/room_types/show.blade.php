@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>ルームタイプID</th>
                <td>{{$room_type->id}}</td>
            </tr>
            <tr>
                <th>ルームタイプ</th>
                <td>{{$room_type->name}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$room_type->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$room_type->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('room_types.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('room_types.edit', $room_type->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop