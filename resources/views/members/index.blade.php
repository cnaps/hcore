@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('members.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>従業員ID</th>
                <th>名前</th>
                {{--<th>ふりがな（姓）</th>--}}
                {{--<th>ふりがな（名）</th>--}}
                <th>アカウント</th>
                {{--<th>郵便番号</th>--}}
                {{--<th>都道府県</th>--}}
                {{--<th>市区町村</th>--}}
                {{--<th>番地</th>--}}
                {{--<th>建物</th>--}}
                {{--<th>入社日</th>--}}
                {{--<th>退社日</th>--}}
                {{--<th>基本給</th>--}}
                {{--<th>交通費</th>--}}
                {{--<th>手当総額</th>--}}
                {{--<th>有給日数</th>--}}
                {{--<th>勤務形態</th>--}}
                <th>報告入力用勤務地</th>
                <th>更新日時</th>
                {{--<th>登録日時</th>--}}
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($members)
            @foreach($members as $member)

                <tr>
                    <td>{{$member->id}}</td>
                    <td>{{$member->last_name}} {{$member->first_name}}</td>
                    {{--<td>{{$member->last_name_kana}}</td>--}}
                    {{--<td>{{$member->first_name_kana}}</td>--}}
                    <td>{{$member->user->name}}</td>
                    {{--<td>{{$member->postal_code}}</td>--}}
                    {{--<td>{{$member->pref}}</td>--}}
                    {{--<td>{{$member->address1}}</td>--}}
                    {{--<td>{{$member->address2}}</td>--}}
                    {{--<td>{{$member->building}}</td>--}}
                    {{--<td>{{$member->first_day}}</td>--}}
                    {{--<td>{{$member->final_day}}</td>--}}
                    {{--<td>{{$member->base_salary}}</td>--}}
                    {{--<td>{{$member->traveling_expenses}}</td>--}}
                    {{--<td>{{$member->allowances}}</td>--}}
                    {{--<td>{{$member->paid_vacation}}</td>--}}
                    {{--<td>{{$member->status->name}}</td>--}}
                    @if($places)
                        <?php $found = 0; ?>
                        @foreach($places as $place)
                            @if($member->place_id2 == $place->id)
                                <td>{{$place->name}}</td>
                                <?php $found = 1; ?>
                            @endif
                        @endforeach
                        @if($found == 0)
                            <td></td>
                        @endif
                    @endif
                    <td>{{$member->updated_at->diffForhumans()}}</td>
                    {{--<td>{{$member->created_at->diffForhumans()}}</td>--}}
                    <td>
                        <div class="pull-right">
                            <a href="{{route('members.show', $member->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('members.edit', $member->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop