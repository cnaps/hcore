@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/members">
        <div class="form-group">
            <label><span style="color: red">*</span>名前：</label>
            <input name="last_name" type="text" class="form-control" placeholder="姓">
            <input name="first_name" type="text" class="form-control" placeholder="名">
        </div>
        <div class="form-group">
            <label>名前（よみがな）：</label>
            <input name="last_name_kana" type="text" class="form-control" placeholder="姓（かな）">
            <input name="first_name_kana" type="text" class="form-control" placeholder="名（かな）">
        </div>
        <div class="form-group">
            <label>電話番号：</label>
            <input name="tel" type="tel" class="form-control" placeholder="電話番号を入力してください。">
        </div>
        <div class="form-group">
            <label>携帯番号：</label>
            <input name="tel_mp" type="tel" class="form-control" placeholder="携帯番号を入力してください。">
        </div>
        <div class="form-group">
            <label><span style="color: red">*</span>アカウント：</label>
            <select name="user_id" class="form-control">
                @if($users)
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}({{$user->email}})</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>郵便番号：</label>
            <input name="postal_code" type="number" class="form-control" placeholder="郵便番号を入力して下さい。">
        </div>
        <div class="form-group">
            <label>都道府県：</label>
            <input name="pref" type="text" class="form-control" placeholder="都道府県を入力して下さい。">
        </div>
        <div class="form-group">
            <label>市区町村：</label>
            <input name="address1" type="text" class="form-control" placeholder="市区町村を入力して下さい。">
        </div>
        <div class="form-group">
            <label>番地：</label>
            <input name="address2" type="text" class="form-control" placeholder="番地を入力して下さい。">
        </div>
        <div class="form-group">
            <label>建物：</label>
            <input name="building" type="text" class="form-control" placeholder="建物を入力して下さい。">
        </div>
        <div class="form-group">
            <label>基本給：</label>
            <input name="base_salary" type="number" class="form-control" placeholder="基本給を入力して下さい。">
        </div>
        <div class="form-group">
            <label>交通費片道：</label>
            <input name="traveling_expenses" type="number" class="form-control" placeholder="交通費片道を入力して下さい。">
        </div>
        <div class="form-group">
            <label>時給：</label>
            <input name="hourly_wage" type="number" class="form-control" placeholder="時給を入力して下さい。">
        </div>
        <div class="form-group">
            <label>手当総額：</label>
            <input name="allowances" type="number" class="form-control" placeholder="手当総額を入力して下さい。">
        </div>
        <div class="form-group">
            <label>年間有給日数：</label>
            <input name="paid_vacation" type="number" class="form-control" placeholder="年間有給日数を入力して下さい。">
        </div>
        <div class="form-group">
            <label>入社日：</label>
            <input name="first_day" type="date" class="form-control" placeholder="入社日">
        </div>
        <div class="form-group">
            <label>退社日：</label>
            <input name="final_day" type="date" class="form-control" placeholder="退社日">
        </div>
        <div class="form-group">
            <label><span style="color: red">*</span>ステータス：</label>
            <select name="status_id" class="form-control">
                @if($statuses)
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label><span style="color: red">*</span>報告入力用　勤務地：</label>
            <select name="place_id2" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <input name="pass" type="hidden" value="0">
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('members.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop