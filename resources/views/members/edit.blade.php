@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/members/{{$member->id}}">
        <div class="form-group">
            <label><span style="color: red">*</span>名前：</label>
            <input name="last_name" type="text" class="form-control" value="{{$member->last_name}}" placeholder="姓">
            <input name="first_name" type="text" class="form-control" value="{{$member->first_name}}" placeholder="名">
        </div>
        <div class="form-group">
            <label>名前（よみがな）：</label>
            <input name="last_name_kana" type="text" class="form-control" value="{{$member->last_name_kana}}" placeholder="姓（かな）">
            <input name="first_name_kana" type="text" class="form-control" value="{{$member->first_name_kana}}" placeholder="名（かな）">
        </div>
        <div class="form-group">
            <label>電話番号：</label>
            <input name="tel" type="tel" class="form-control" value="{{$member->tel}}" placeholder="電話番号を入力してください。">
        </div>
        <div class="form-group">
            <label>携帯番号：</label>
            <input name="tel_mp" type="tel" class="form-control" value="{{$member->tel_mp}}" placeholder="携帯番号を入力してください。">
        </div>
        <div class="form-group">
            <label><span style="color: red">*</span>アカウント：</label>
            <select name="user_id" class="form-control">
                @if($users)
                    @foreach($users as $user)
                        <option value="{{$user->id}}"
                                @if ($user->id == $member->user_id)
                                selected="selected"
                                @endif
                        >{{$user->name}}({{$user->email}})</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>郵便番号：</label>
            <input name="postal_code" type="number" class="form-control" value="{{$member->postal_code}}" placeholder="郵便番号を入力して下さい。">
        </div>
        <div class="form-group">
            <label>都道府県：</label>
            <input name="pref" type="text" class="form-control" value="{{$member->pref}}" placeholder="都道府県を入力して下さい。">
        </div>
        <div class="form-group">
            <label>市区町村：</label>
            <input name="address1" type="text" class="form-control" value="{{$member->address1}}" placeholder="市区町村を入力して下さい。">
        </div>
        <div class="form-group">
            <label>番地：</label>
            <input name="address2" type="text" class="form-control" value="{{$member->address2}}" placeholder="番地を入力して下さい。">
        </div>
        <div class="form-group">
            <label>建物：</label>
            <input name="building" type="text" class="form-control" value="{{$member->building}}" placeholder="建物を入力して下さい。">
        </div>
        <div class="form-group">
            <label>基本給：</label>
            <input name="base_salary" type="number" class="form-control" value="{{$member->base_salary}}" placeholder="基本給を入力して下さい。">
        </div>
        <div class="form-group">
            <label>交通費片道：</label>
            <input name="traveling_expenses" type="number" class="form-control" value="{{$member->traveling_expenses}}" placeholder="交通費を入力して下さい。">
        </div>
        <div class="form-group">
            <label>時給：</label>
            <input name="hourly_wage" type="number" class="form-control" value="{{$member->hourly_wage}}" placeholder="時給を入力して下さい。">
        </div>
        <div class="form-group">
            <label>手当総額：</label>
            <input name="allowances" type="number" class="form-control" value="{{$member->allowances}}" placeholder="手当総額を入力して下さい。">
        </div>
        <div class="form-group">
            <label>年間有給日数：</label>
            <input name="paid_vacation" type="number" class="form-control" value="{{$member->paid_vacation}}" placeholder="年間有給日数を入力して下さい。">
        </div>
        <div class="form-group">
            <label>入社日：</label>
            <input name="first_day" type="date" class="form-control" value="{{$member->first_day}}" placeholder="入社日">
        </div>
        <div class="form-group">
            <label>退社日：</label>
            <input name="final_day" type="date" class="form-control" value="{{$member->final_day}}" placeholder="退社日">
        </div>
        <div class="form-group">
            <label>パスコード：</label>
            <input name="pass" type="number" class="form-control" value="{{$member->pass}}" placeholder="パスコードを入力してください。">
        </div>
        <div class="form-group">
            <label><span style="color: red">*</span>ステータス：</label>
            <select name="status_id" class="form-control">
                @if($statuses)
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}"
                                @if ($status->id == $member->status_id)
                                selected="selected"
                                @endif
                        >{{$status->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label><span style="color: red">*</span>報告入力用　勤務地：</label>
            <select name="place_id2" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == $member->place_id2)
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="pull-right">
            <a href="{{route('members.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('members.show', $member->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop