@extends('layouts.app')
@section('content')
    <script type="text/javascript">
        $(function(){
            var val = 0;
            $('[name=place_id]').change(function() {
                val = $('[name=place_id]').val();
                window.location.href = "/index_shift/" + val;
            });
        });
    </script>
    <h1>一覧</h1>
    <form method="post" action="/update_shift">
        <div class="form-group">
            <label>物件選択：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == $place_id)
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{csrf_field()}}
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>名前</th>
                    <th>月</th>
                    <th>火</th>
                    <th>水</th>
                    <th>木</th>
                    <th>金</th>
                    <th>土</th>
                    <th>日</th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 0; ?>
            @if($members)
                @foreach($members as $member)
                    <?php $i++; ?>
                    <input type="hidden" name="member_id{{$i}}" value="{{$member->id}}">
                    <tr>
                        <td>{{$member->last_name}} {{$member->first_name}}</td>
                        @if($member->shift_monday == 0)
                            <td><input name="monday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="monday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                        @if($member->shift_tuesday == 0)
                            <td><input name="tuesday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="tuesday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                        @if($member->shift_wednesday == 0)
                            <td><input name="wednesday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="wednesday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                        @if($member->shift_thursday == 0)
                            <td><input name="thursday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="thursday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                        @if($member->shift_friday == 0)
                            <td><input name="friday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="friday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                        @if($member->shift_saturday == 0)
                            <td><input name="saturday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="saturday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                        @if($member->shift_sunday == 0)
                            <td><input name="sunday{{$i}}" type="checkbox" value="1"/></td>
                        @else
                            <td><input name="sunday{{$i}}" type="checkbox" value="1" checked="checked"/></td>
                        @endif
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <input type="hidden" name="count" value="{{$i}}">
        <button type="submit" class="pull-right btn btn-primary btn-lg" style="margin-top:20px;margin-bottom:150px;">登録</button>
    </form>
@stop