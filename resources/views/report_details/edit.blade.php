@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/report_details/{{$report_detail->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == $report_detail->place_id)
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>名前：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == $report_detail->member_id)
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" type="date" class="form-control" value="<?php echo $report_detail->date->format('Y-m-d'); ?>">
        </div>
        <div class="form-group">
            <label>出勤：</label>
            @if($report_detail->working_flg == 1)
                <input type="checkbox" name="working_flg" value="1" checked> ※出勤時クリック
            @else
                <input type="checkbox" name="working_flg" value="1"> ※出勤時クリック
            @endif
        </div>
        <div class="form-group">
            <label>鍵チェック：</label>
            @if($report_detail->key_check == 1)
                <input type="checkbox" name="key_check" value="1" checked>
            @else
                <input type="checkbox" name="key_check" value="1">
            @endif
        </div>
        <div class="form-group">
            <label>作業室数合計：</label>
            <input name="make_total" value="{{$report_detail->make_total}}" type="number" class="form-control" placeholder="作業室数合計を入力してください。">
        </div>
        <div class="form-group">
            <label>階1：</label>
            <input name="floor1" value="{{$report_detail->floor1}}" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数1：</label>
            <input name="room_count1" value="{{$report_detail->room_count1}}" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階2：</label>
            <input name="floor2" value="{{$report_detail->floor2}}" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数2：</label>
            <input name="room_count2" value="{{$report_detail->room_count2}}" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階3：</label>
            <input name="floor3" value="{{$report_detail->floor3}}" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数3：</label>
            <input name="room_count3" value="{{$report_detail->room_count3}}" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階4：</label>
            <input name="floor4" value="{{$report_detail->floor4}}" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数4：</label>
            <input name="room_count4" value="{{$report_detail->room_count4}}" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階5：</label>
            <input name="floor5" value="{{$report_detail->floor5}}" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数5：</label>
            <input name="room_count5" value="{{$report_detail->room_count5}}" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>その他：</label>
            <input name="other" value="{{$report_detail->other}}" type="text" class="form-control" placeholder="その他を入力してください。">
        </div>
        <div class="pull-right">
            <a href="{{route('report_details.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('report_details.show', $report_detail->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop