@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('report_details.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>報告詳細ID</th>
                <th>物件名</th>
                <th>当番者</th>
                <th>日付</th>
                <th>勤務</th>
                <th>鍵確認</th>
                <th>作業室数計</th>
                <th>その他</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($report_details)
            @foreach($report_details as $report_detail)
                <tr>
                    <td>{{$report_detail->id}}</td>
                    <td>{{$report_detail->place->name}}</td>
                    <td>{{$report_detail->member->last_name}} {{$report_detail->member->first_name}}</td>
                    <td><?php echo $report_detail->date->format('Y-m-d'); ?></td>
                    <td><?php if ($report_detail->working_flg == 0){echo "";}else{echo "出勤";} ?></td>
                    <td><?php if ($report_detail->key_check == 0){echo "未確認";}else{echo "確認済";} ?></td>
                    <td>{{$report_detail->make_total}}</td>
                    <td>{{$report_detail->other}}</td>
                    <td>{{$report_detail->updated_at->diffForhumans()}}</td>
                    <td>{{$report_detail->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('report_details.show', $report_detail->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('report_details.edit', $report_detail->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop