@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/report_details">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>名前：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" type="date" class="form-control" value="<?php $today = Carbon\Carbon::now(); echo $today->format('Y-m-d'); ?>">
        </div>
        <div class="form-group">
            <label>出勤：</label>
            <input type="checkbox" name="working_flg" value="1"> ※出勤時クリック
        </div>
        <div class="form-group">
            <label>鍵チェック：</label>
            <input type="checkbox" name="key_check" value="1">
        </div>
        <div class="form-group">
            <label>作業室数合計：</label>
            <input name="make_total" type="number" class="form-control" placeholder="作業室数合計を入力してください。">
        </div>
        <div class="form-group">
            <label>階1：</label>
            <input name="floor1" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数1：</label>
            <input name="room_count1" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階2：</label>
            <input name="floor2" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数2：</label>
            <input name="room_count2" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階3：</label>
            <input name="floor3" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数3：</label>
            <input name="room_count3" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階4：</label>
            <input name="floor4" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数4：</label>
            <input name="room_count4" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>階5：</label>
            <input name="floor5" type="number" class="form-control" placeholder="階を入力してください。">
        </div>
        <div class="form-group">
            <label>室数5：</label>
            <input name="room_count5" type="number" class="form-control" placeholder="室数を入力してください。">
        </div>
        <div class="form-group">
            <label>その他：</label>
            <input name="other" type="text" class="form-control" placeholder="その他を入力してください。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('report_details.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop