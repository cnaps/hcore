@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>物件ID</th>
                <td>{{$condition->id}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$condition->condition}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$condition->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$condition->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('conditions.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('conditions.edit', $condition->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop