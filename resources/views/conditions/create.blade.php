@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/conditions">
        <div class="form-group">
            <label>状態：</label>
            <input name="condition" type="text" class="form-control" placeholder="状態を入力して下さい。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('places.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop