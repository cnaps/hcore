@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/conditions/{{$condition->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>状態：</label>
            <input name="condition" type="text" class="form-control" value="{{$condition->condition}}" placeholder="状態を入力して下さい。">
        </div>
        <div class="pull-right">
            <a href="{{route('conditions.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('conditions.show', $condition->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop