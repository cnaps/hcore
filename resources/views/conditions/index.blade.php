@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('conditions.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>状態ID</th>
                <th>状態</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($conditions)
            @foreach($conditions as $condition)

                <tr>
                    <td>{{$condition->id}}</td>
                    <td>{{$condition->condition}}</td>
                    <td>{{$condition->updated_at->diffForhumans()}}</td>
                    <td>{{$condition->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('conditions.show', $condition->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('conditions.edit', $condition->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop