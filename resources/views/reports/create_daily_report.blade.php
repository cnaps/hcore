@extends('layouts.app')
@section('content')
    <script type="text/javascript">
        $(function(){
            $('.cal').change(function () {
                var PageAll = 0;
                var member_count = $('input[name=member_count]').val();
                member_count = Number(member_count);
                for (var count = 0; count < member_count; count++) {
                    var val1 = $('input[name=room_count1_'+ count +']').val();
                    var val2 = $('input[name=room_count2_'+ count +']').val();
                    var val3 = $('input[name=room_count3_'+ count +']').val();
                    var val4 = $('input[name=room_count4_'+ count +']').val();
                    var val5 = $('input[name=room_count5_'+ count +']').val();
                    var ItemAll = Number(val1) + Number(val2) + Number(val3) + Number(val4) + Number(val5);
                    $('span[name=make_count'+ count +']').text(ItemAll);
                    $('input[name=make_total'+ count +']').attr('value',ItemAll);
                    PageAll = PageAll + ItemAll;
                }
                $('p[name=cleaned_rooms_quantity]').text(PageAll);
                $('input[name=cleaned_rooms_quantity]').attr('value',PageAll);
            });

            $('.working').change(function () {
                var count = $(this).data('count');
                var working = $('input[name=working_flg'+ count +']:checked').val();
                if (working != '1'){
                    working = Number(0);
                }
                working = Number(working);
                if (working == 0) {
                    // 確認ダイアログの表示
//                    if(window.confirm('以下の入力がクリアされますが宜しいですか？')){
//                    } else {
//                    }
                    $('span[name=key_label'+ count +']').hide();
                    $('span[name=total_label'+ count +']').hide();
                    $('input[name=key_check'+ count +']').hide();
                    $('span[name=make_count'+ count +']').hide();
                    $('div[name=rooms_label'+ count +']').hide();
                    $('table[name=table_room'+ count +']').hide();
                    $('div[name=other_label'+ count +']').hide();
                    $('textarea[name=other'+ count +']').hide();
                }else{
                    $('span[name=key_label'+ count +']').show();
                    $('span[name=total_label'+ count +']').show();
                    $('input[name=key_check'+ count +']').show();
                    $('span[name=make_count'+ count +']').show();
                    $('div[name=rooms_label'+ count +']').show();
                    $('table[name=table_room'+ count +']').show();
                    $('div[name=other_label'+ count +']').show();
                    $('textarea[name=other'+ count +']').show();
                }
            });
        });

        $(document).ready(function(){
            var member_count = $('input[name=member_count]').val();
            member_count = Number(member_count);
            for (var count = 0; count < member_count; count++){
                var working = $('input[name=working_flg'+ count +']:checked').val();
                if (working != '1'){
                    working = Number(0);
                }
                working = Number(working);
                if (working == 0) {
                    $('span[name=key_label'+ count +']').hide();
                    $('span[name=total_label'+ count +']').hide();
                    $('input[name=key_check'+ count +']').hide();
                    $('span[name=make_count'+ count +']').hide();
                    $('div[name=rooms_label'+ count +']').hide();
                    $('table[name=table_room'+ count +']').hide();
                    $('div[name=other_label'+ count +']').hide();
                    $('textarea[name=other'+ count +']').hide();
                }else{
                    $('span[name=key_label'+ count +']').show();
                    $('span[name=total_label'+ count +']').show();
                    $('input[name=key_check'+ count +']').show();
                    $('span[name=make_count'+ count +']').show();
                    $('div[name=rooms_label'+ count +']').show();
                    $('table[name=table_room'+ count +']').show();
                    $('div[name=other_label'+ count +']').show();
                    $('textarea[name=other'+ count +']').show();
                }
            }
        });
    </script>
    <style>
        input[type=checkbox] {
            transform: scale(5);
            margin:50px;
            background-color: #e0f4fc;
        }
        input[type=tel]{
            height:100px;
            font-size: 50px;
            font-weight: bold;
            text-align: center;
            background-color: #e0f4fc;
        }
        input[type=text]{
            font-size: 50px;
            height:100px;
            background-color: #e0f4fc;
        }
        select[name="member_id"]{
            font-size: 50px;
            height:100px;
        }
        textarea[type=text]{
            font-size: 50px;
            background-color: #e0f4fc;
        }
        hr {
            border-top: 3px double #bbb;
        }
        td.top{
            padding-bottom: 5px;
        }
        td.bottom{
            padding-top: 5px;
        }
    </style>
    <h2 style="font-size: 50px;font-weight: bold">パイオニア・システム<br>客室販売</h2>
    <h3 style="font-size: 40px">{{$today}}</h3>
    <h3 style="text-align: right">場所： <span style="font-size: 50px;font-weight: bold">{{$place->name}}</span></h3>
    <form method="post" action="/check_daily_report">
        {!! csrf_field() !!}
        <input type="hidden" name="member_count" value="{{$member_count}}">
        <input name="place_id" type="hidden" value="{{$place->id}}">
        <input name="date" value="{{$today2}}" type="hidden">
        @if($members)
            <table style="table-layout: fixed;width: 100%;margin-top: 50px;">
                <tr>
                    <td>
                        <?php $count = 0; ?>
                        @foreach($members as $member)
                            <div>
                                <input type="hidden" name="make_total{{$count}}" value="0">
                                <hr>
                                <input type="hidden" name="member_id{{$count}}" value="{{$member->id}}">
                                <div name="name{{$count}}" style="font-weight: bold;font-size: 70px;">{{$member->last_name}} {{$member->first_name}}</div>
                                <table style="margin-top:30px;table-layout: fixed;width: 100%">
                                    <tr>
                                        <td name="working_label{{$count}}" align="left"><span style="font-size: 30px;font-weight: bold">出勤チェック</span></td>
                                        <td align="left"><span name="key_label{{$count}}" style="font-size: 30px;font-weight: bold">鍵チェック</span></td>
                                        <td align="right"><span name="total_label{{$count}}" style="font-size: 30px;font-weight: bold">作業室数合計</span></td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <input class="working" type="checkbox" name="working_flg{{$count}}" value="1" data-count="{{$count}}">
                                        </td>
                                        <td align="left">
                                            <input class="key" name="key_check{{$count}}" type="checkbox" value="1" data-count="{{$count}}">
                                        </td>
                                        <td align="right"><span name="make_count{{$count}}" style="font-weight: bold;font-size: 70px;">0</span></td>
                                    </tr>
                                </table>
                                <div name="rooms_label{{$count}}" style="margin-top:30px;font-size: 30px;font-weight: bold">メイクした階と室数：</div>
                                <table name="table_room{{$count}}" style="table-layout: fixed;width: 100%">
                                    <tr>
                                        <td class="top" colspan="2"><input name="floor1_{{$count}}" type="tel" class="form-control" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">階</td>
                                        <td class="top" colspan="2"><input name="floor2_{{$count}}" type="tel" class="form-control" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">階</td>
                                        <td class="top" colspan="2"><input name="floor3_{{$count}}" type="tel" class="form-control" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">階</td>
                                        <td class="top" colspan="2"><input name="floor4_{{$count}}" type="tel" class="form-control" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">階</td>
                                        <td class="top" colspan="2"><input name="floor5_{{$count}}" type="tel" class="form-control" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">階</td>
                                    </tr>
                                    <tr>
                                        <td class="bottom" colspan="2"><input name="room_count1_{{$count}}" type="tel" class="form-control cal" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">室</td>
                                        <td class="bottom" colspan="2"><input name="room_count2_{{$count}}" type="tel" class="form-control cal" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">室</td>
                                        <td class="bottom" colspan="2"><input name="room_count3_{{$count}}" type="tel" class="form-control cal" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">室</td>
                                        <td class="bottom" colspan="2"><input name="room_count4_{{$count}}" type="tel" class="form-control cal" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">室</td>
                                        <td class="bottom" colspan="2"><input name="room_count5_{{$count}}" type="tel" class="form-control cal" data-count="{{$count}}"></td>
                                        <td style="padding-left: 5px;font-size:20px;">室</td>
                                    </tr>
                                </table>
                                <div name="other_label{{$count}}" style="margin-top: 30px;font-size: 30px;font-weight: bold">その他：</div>
                                <textarea rows="3" name="other{{$count}}" type="text" class="form-control" placeholder=""></textarea>
                                <?php $count++; ?>
                            </div>
                        @endforeach
                    </td>
                </tr>
            </table>
        @endif
        <hr>
        <table style="table-layout: fixed;width: 100%;">
            <tr>
                <td style="padding-right: 10px;">
                    <div class="form-group">
                        <label style="font-size: 30px;font-weight: bold">メイク指示数：</label>
                        <input name="ordered_rooms_quantity" type="tel" class="form-control">
                    </div>
                </td>
                <td style="padding-left: 10px;">
                    <div class="form-group">
                        <label style="font-size: 30px;font-weight: bold">作業室数合計：</label>
                        <p name="cleaned_rooms_quantity" type="number" style="font-weight:bold;font-size: 50px;width: 100%;text-align: center">0</p>
                        <input type="hidden" name="cleaned_rooms_quantity">
                    </div>
                </td>
            </tr>
        </table>
        <div class="form-group" style="margin-top: 50px;">
            <label style="font-size: 30px;font-weight: bold">不要室番：</label>
            <input name="unused_room_num" type="text" class="form-control">
        </div>
        <div class="form-group" style="margin-top: 50px;">
            <label style="font-size: 30px;font-weight: bold">伝達事項：</label>
            <textarea type="text" name="note" rows="4" class="form-control" id="InputTextarea"></textarea>
        </div>
        <div class="form-group" style="margin-top: 50px;">
            <label style="font-size: 30px;font-weight: bold">当番者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <input name="unit_price" type="hidden" value="{{$place->unit_price}}">
        <div style="margin-bottom: 100px;margin-top:80px;width: 100%;text-align: center">
            <p style="text-align: center;font-size: 20px">入力後【確認】ボタンを押してください。</p>
            <button type="submit" class="btn btn-primary" style="font-size: 50px;padding: 50px 100px 50px 100px">確 認</button>
        </div>
    </form>
@stop