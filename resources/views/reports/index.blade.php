@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('reports.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>報告ID</th>
                <th>物件名</th>
                <th>当番者</th>
                <th>室単価</th>
                <th>日付</th>
                <th>不要室番</th>
                <th>伝達事項</th>
                <th>メイク指示数</th>
                <th>作業室数</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($reports)
            @foreach($reports as $report)
                <tr>
                    <td>{{$report->id}}</td>
                    <td>{{$report->member->id}}</td>
                    {{--<td>{{$report->member->user->place->name}}</td>--}}
{{--                    <td>{{$report->member->last_name}} {{$report->member->first_name}}</td>--}}
                    <td>{{$report->unit_price}}</td>
                    <td><?php echo $report->date->format('Y-m-d'); ?></td>
                    <td>{{$report->unused_room_num}}</td>
                    <td>{{$report->note}}</td>
                    <td>{{$report->ordered_rooms_quantity}}</td>
                    <td>{{$report->cleaned_rooms_quantity}}</td>
                    {{--<td>{{$report->updated_at->diffForhumans()}}</td>--}}
                    {{--<td>{{$report->created_at->diffForhumans()}}</td>--}}
                    <td>
                        <div class="pull-right">
                            <a href="{{route('reports.show', $report->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('reports.edit', $report->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop