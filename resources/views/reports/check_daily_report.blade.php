@extends('layouts.app')
@section('content')
    <h2 style="font-size: 50px;font-weight: bold">入力確認</h2>
    <h3 style="font-size: 40px">{{$today}}</h3>
    <h3 style="text-align: right">場所： <span style="font-size: 50px;font-weight: bold">{{$place->name}}</span></h3>
    <div style="font-size: 20px;color: blue;font-weight: bold">以下の内容で送信します。よろしいですか？<br>良い場合は【送信】ボタンを押してください。<br><span style="color: black">※送信後に修正することも出来ます。</span></div>
    <form method="post" action="{{route('update_daily_report')}}">
        {!! csrf_field() !!}
        <table style="table-layout: fixed;width: 100%;margin-top: 50px;">
            <?php $notZero = 0; ?>
            @for($i = 0;$i < $request->input('member_count');$i++)
                @if($request2[$i]["working_flg"] == 1)
                    <tr>
                        <td colspan="5"><hr></td>
                    </tr>
                    <tr>
                        <td colspan="5" style="font-size: 50px;font-weight: bold">{{$request2[$i]["member_name"]}}</td>

                    </tr>
                    <tr>
                        <td colspan="5" style="font-size: 30px">
                            @if($request2[$i]["key_check"] == 1)
                                鍵チェック： <strong style="color: green">確認済み</strong>
                            @else
                                鍵チェック： <strong style="color: red">未確認</strong>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="font-size: 30px">作業室合計： <strong>{{$request2[$i]["make_total"]}}</strong> 室</td>
                    </tr>
                    <tr>
                        <td colspan="5" style="font-size: 30px">作業階と室数：
                            @if($request2[$i]["room_count1"] != 0)
                                <strong>{{$request2[$i]["floor1"]}}</strong> 階 <strong>{{$request2[$i]["room_count1"]}}</strong> 室 /
                            @endif
                            @if($request2[$i]["room_count2"] != 0)
                                <strong>{{$request2[$i]["floor2"]}}</strong> 階 <strong>{{$request2[$i]["room_count2"]}}</strong> 室 /
                            @endif
                            @if($request2[$i]["room_count3"] != 0)
                                <strong>{{$request2[$i]["floor3"]}}</strong> 階 <strong>{{$request2[$i]["room_count3"]}}</strong> 室 /
                            @endif
                            @if($request2[$i]["room_count4"] != 0)
                                <strong>{{$request2[$i]["floor4"]}}</strong> 階 <strong>{{$request2[$i]["room_count4"]}}</strong> 室 /
                            @endif
                            @if($request2[$i]["room_count5"] != 0)
                                <strong>{{$request2[$i]["floor5"]}}</strong> 階 <strong>{{$request2[$i]["room_count5"]}}</strong> 室 /
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="font-size: 30px">その他： <strong>{{$request2[$i]["other"]}}</strong></td>
                    </tr>
                    <input type="hidden" name="working_flg{{$i}}" value="{{$request2[$i]["working_flg"]}}">
                    <input type="hidden" name="member_id{{$i}}" value="{{$request2[$i]["member_id"]}}">
                    <input type="hidden" name="key_check{{$i}}" value="{{$request2[$i]["key_check"]}}">
                    <input type="hidden" name="make_total{{$i}}" value="{{$request2[$i]["make_total"]}}">
                    <input type="hidden" name="floor1_{{$i}}" value="{{$request2[$i]["floor1"]}}">
                    <input type="hidden" name="floor2_{{$i}}" value="{{$request2[$i]["floor2"]}}">
                    <input type="hidden" name="floor3_{{$i}}" value="{{$request2[$i]["floor3"]}}">
                    <input type="hidden" name="floor4_{{$i}}" value="{{$request2[$i]["floor4"]}}">
                    <input type="hidden" name="floor5_{{$i}}" value="{{$request2[$i]["floor5"]}}">
                    <input type="hidden" name="room_count1_{{$i}}" value="{{$request2[$i]["room_count1"]}}">
                    <input type="hidden" name="room_count2_{{$i}}" value="{{$request2[$i]["room_count2"]}}">
                    <input type="hidden" name="room_count3_{{$i}}" value="{{$request2[$i]["room_count3"]}}">
                    <input type="hidden" name="room_count4_{{$i}}" value="{{$request2[$i]["room_count4"]}}">
                    <input type="hidden" name="room_count5_{{$i}}" value="{{$request2[$i]["room_count5"]}}">
                    <input type="hidden" name="other{{$i}}" value="{{$request2[$i]["other"]}}">
                    <?php $notZero = 1; ?>
                @else
                    <input type="hidden" name="working_flg{{$i}}" value="{{$request2[$i]["working_flg"]}}">
                    <input type="hidden" name="member_id{{$i}}" value="{{$request2[$i]["member_id"]}}">
                    <input type="hidden" name="key_check{{$i}}" value="{{$request2[$i]["key_check"]}}">
                    <input type="hidden" name="make_total{{$i}}" value="{{$request2[$i]["make_total"]}}">
                    <input type="hidden" name="floor1_{{$i}}" value="{{$request2[$i]["floor1"]}}">
                    <input type="hidden" name="floor2_{{$i}}" value="{{$request2[$i]["floor2"]}}">
                    <input type="hidden" name="floor3_{{$i}}" value="{{$request2[$i]["floor3"]}}">
                    <input type="hidden" name="floor4_{{$i}}" value="{{$request2[$i]["floor4"]}}">
                    <input type="hidden" name="floor5_{{$i}}" value="{{$request2[$i]["floor5"]}}">
                    <input type="hidden" name="room_count1_{{$i}}" value="{{$request2[$i]["room_count1"]}}">
                    <input type="hidden" name="room_count2_{{$i}}" value="{{$request2[$i]["room_count2"]}}">
                    <input type="hidden" name="room_count3_{{$i}}" value="{{$request2[$i]["room_count3"]}}">
                    <input type="hidden" name="room_count4_{{$i}}" value="{{$request2[$i]["room_count4"]}}">
                    <input type="hidden" name="room_count5_{{$i}}" value="{{$request2[$i]["room_count5"]}}">
                    <input type="hidden" name="other{{$i}}" value="{{$request2[$i]["other"]}}">
                @endif
            @endfor
        </table>
        @if($notZero == 0)
            <div style="padding:20px;text-align: center;color: blue;font-size: 20px;font-weight: bold">データが１件も入力されていません。</div>
        @endif
        <hr>
        <p style="font-weight:bold;font-size: 30px;width: 100%;">メイク指示数： {{$request->ordered_rooms_quantity}} 室</p>
        <p style="font-weight:bold;font-size: 30px;width: 100%;">作業室数合計： {{$request->cleaned_rooms_quantity}} 室</p>
        <p style="font-weight:bold;font-size: 30px;width: 100%;">不要室番：{{$request->unused_room_num}}</p>
        <p style="font-weight:bold;font-size: 30px;width: 100%;">伝達事項：{{$request->note}}</p>
        <p style="font-weight:bold;font-size: 30px;width: 100%;">当番者：{{$member_toban->last_name}} {{$member_toban->first_name}}</p>
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{$request->id}}">
        <input type="hidden" name="member_count" value="{{$request->member_count}}">
        <input type="hidden" name="place_id" value="{{$request->place_id}}">
        <input type="hidden" name="date" value="{{$request->date}}">
        <input type="hidden" name="ordered_rooms_quantity" value="{{$request->ordered_rooms_quantity}}">
        <input type="hidden" name="cleaned_rooms_quantity" value="{{$request->cleaned_rooms_quantity}}">
        <input type="hidden" name="unused_room_num" value="{{$request->unused_room_num}}">
        <input type="hidden" name="note" value="{{$request->note}}">
        <input type="hidden" name="member_id" value="{{$request->member_id}}">
        <input type="hidden" name="unit_price" value="{{$request->unit_price}}">
        <div style="margin-bottom: 100px;margin-top:80px;width: 100%;text-align: center">
            <div style="font-size: 20px;padding:5px;">確認後【送信】ボタンを押してください。</div>
            <a class="btn btn-default" style="font-size: 50px;padding: 25px 50px 25px 50px" onClick="history.back(); return false;">戻 る</a>
            <button type="submit" class="btn btn-primary" style="margin-left:20px;font-size: 50px;padding: 25px 50px 25px 50px">送 信</button>
        </div>
    </form>
@stop