@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>報告ID</th>
                <td>{{$report->id}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$report->place->name}}</td>
            </tr>
            <tr>
                <th>報告者</th>
                <td>{{$report->member->last_name}} {{$report->member->first_name}}</td>
            </tr>
            <tr>
                <th>室単価</th>
                <td>{{$report->unit_price}}</td>
            </tr>
            <tr>
                <th>日付</th>
                <td><?php echo $report->date->format('Y-m-d'); ?></td>
            </tr>
            <tr>
                <th>不要室番</th>
                <td>{{$report->unused_room_num}}</td>
            </tr>
            <tr>
                <th>伝達事項</th>
                <td>{{$report->note}}</td>
            </tr>
            <tr>
                <th>メイク指示数</th>
                <td>{{$report->ordered_rooms_quantity}}</td>
            </tr>
            <tr>
                <th>作業室数</th>
                <td>{{$report->cleaned_rooms_quantity}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$report->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$report->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('reports.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('reports.edit', $report->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop