@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/reports/{{$report->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == old('place_id'))
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>当番者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == $report->member_id)
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>室単価：</label>
            <input name="unit_price" value="{{$report->unit_price}}" type="number" class="form-control">
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" value="<?php use Carbon\Carbon; echo $report->date->format('Y-m-d'); ?>" type="date" class="form-control">
        </div>
        <div class="form-group">
            <label>不要室番：</label>
            <input name="unused_room_num" value="{{$report->unused_room_num}}" type="text" placeholder="不要室番を入力してください。" class="form-control">
        </div>
        <div class="form-group">
            <label>伝達事項：</label>
            <input name="note" value="{{$report->note}}" type="text" class="form-control" placeholder="伝達事項を入力してください。">
        </div>
        <div class="form-group">
            <label>メイク指示数：</label>
            <input name="ordered_rooms_quantity" value="{{$report->ordered_rooms_quantity}}" type="number" class="form-control" placeholder="メイクを指示された数を入力してください。">
        </div>
        <div class="form-group">
            <label>メイク実数：</label>
            <input name="cleaned_rooms_quantity" value="{{$report->cleaned_rooms_quantity}}" type="number" class="form-control" placeholder="実際にメイクした実数を入力してください。">
        </div>
        <div class="pull-right">
            <a href="{{route('reports.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('reports.show', $report->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop