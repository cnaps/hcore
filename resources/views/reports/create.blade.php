@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/reports">
        <div class="form-group">
            <label>物件名：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}">{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>当番者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>室単価：</label>
            <input name="unit_price" type="number" class="form-control" placeholder="室単価を入力してください。">
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" type="date" class="form-control" value="<?php $today = Carbon\Carbon::now(); echo $today->format('Y-m-d'); ?>">
        </div>
        <div class="form-group">
            <label>不要室番：</label>
            <input name="unused_room_num" type="text" placeholder="不要室番を入力してください。" class="form-control">
        </div>
        <div class="form-group">
            <label>伝達事項：</label>
            <textarea name="note" placeholder="伝達事項を入力してください。" rows="4" class="form-control" id="InputTextarea"></textarea>
        </div>
        <div class="form-group">
            <label>メイク指示数：</label>
            <input name="ordered_rooms_quantity" type="number" class="form-control" placeholder="メイクを指示された数を入力してください。">
        </div>
        <div class="form-group">
            <label>作業室数：</label>
            <input name="cleaned_rooms_quantity" type="number" class="form-control" placeholder="実際にメイクした実数を入力してください。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('reports.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop