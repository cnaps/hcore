@extends('layouts.app')
@section('content')
    <h1>業務報告集計表</h1>
    <form method="post" action="{{route('index_daily_report2')}}">
        <div class="container-fluid">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="form-group">
                        <label>物件選択：</label>
                        <select name="place_id" class="form-control">
                            @if($places)
                                @foreach($places as $place)
                                    <option value="{{$place->id}}"
                                            @if ($place->id == $place_id)
                                            selected="selected"
                                            @endif
                                    >{{$place->name}}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>表示月：</label>
                        <input name="month" value="{{$date_m}}" type="month" class="form-control">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>クリックで更新</label>
                        <button name="submit" type="submit" class="form-control">変更</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div style="overflow-x: scroll">
        <table id="Sheet" class="table table-hover table-bordered table-striped" style="table-layout: fixed;font-size: 14px;">
            <thead>
                <tr>
                    <th width="200"></th>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php   $year = $date->year;
                        $day = \Carbon\Carbon::parse($year.'-'.$month.'-'.$i)->dayOfWeek;
                        ?>
                        <th align="center" valign="center" width="150" <?php
                            $color = "";
                            if ($day == 0 ){
                                $color = "#ffd6df";
                                echo 'style="background-color:'.$color.';text-align: center"';
                            }else if ($day == 6){
                                $color = "#e0f1ff";
                                echo 'style="background-color:'.$color.';text-align: center"';
                            }else{
                                echo 'style="text-align: center"';
                            }
                            ?>>{{$month}}月{{$i}}日({{$week[$day]}})</th>
                    @endfor
                </tr>
            </thead>
            <tbody>
                <?php $nothing = 0; ?>
                @if($report_details_member)
                    @foreach($report_details_member as $report_detail_member)
                        @foreach($members as $member)
                            @if($member->id == $report_detail_member->member_id)
                                <tr>
                                    <td>{{$member->last_name}} {{$member->first_name}}</td>
                                    @for($i = 1;$i <= $dayCount;$i++)
                                        <?php $found = 0; ?>
                                        @if($report_details)
                                            @foreach($report_details as $report_detail)
                                                @if(\Carbon\Carbon::parse($report_detail->date)->day == $i && $report_detail->member_id == $report_detail_member->member_id)
                                                    <td align="center" valign="center">{{$report_detail->make_total}}</td>
                                                    <?php $found = 1; ?>
                                                @endif
                                            @endforeach
                                        @endif
                                        @if($found == 0)
                                            <td></td>
                                        @endif
                                    @endfor
                                </tr>
                                <?php $nothing = 1; ?>
                            @endif
                        @endforeach
                    @endforeach
                @endif
                <tr>
                    <td style="border-top:double 3px;">メイク指示数</td>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php $found = 0; ?>
                        @if($reports)
                            @foreach($reports as $report)
                                @if(\Carbon\Carbon::parse($report->date)->day == $i)
                                    <td style="border-top:double 3px;" align="center" valign="center">{{$report->ordered_rooms_quantity}}</td>
                                    <?php $found = 1; ?>
                                @endif
                            @endforeach
                        @endif
                        @if($found == 0)
                            <td style="border-top:double 3px;"></td>
                        @endif
                    @endfor
                </tr>
                <tr>
                    <td>作業室数合計</td>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php $found = 0; ?>
                        @if($reports)
                            @foreach($reports as $report)
                                @if(\Carbon\Carbon::parse($report->date)->day == $i)
                                    <td align="center" valign="center">{{$report->cleaned_rooms_quantity}}</td>
                                    <?php $found = 1; ?>
                                @endif
                            @endforeach
                        @endif
                        @if($found == 0)
                            <td></td>
                        @endif
                    @endfor
                </tr>
                <tr>
                    <td>不要室番</td>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php $found = 0; ?>
                        @if($reports)
                            @foreach($reports as $report)
                                @if(\Carbon\Carbon::parse($report->date)->day == $i)
                                    <td align="left" valign="center">{{$report->unused_room_num}}</td>
                                    <?php $found = 1; ?>
                                @endif
                            @endforeach
                        @endif
                        @if($found == 0)
                            <td></td>
                        @endif
                    @endfor
                </tr>
                <tr>
                    <td>伝達事項</td>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php $found = 0; ?>
                        @if($reports)
                            @foreach($reports as $report)
                                @if(\Carbon\Carbon::parse($report->date)->day == $i)
                                    <td align="left" valign="center">{{$report->note}}</td>
                                    <?php $found = 1; ?>
                                @endif
                            @endforeach
                        @endif
                        @if($found == 0)
                            <td></td>
                        @endif
                    @endfor
                </tr>
                <tr>
                    <td>室単価</td>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php $found = 0; ?>
                        @if($reports)
                            @foreach($reports as $report)
                                @if(\Carbon\Carbon::parse($report->date)->day == $i)
                                    <td align="center" valign="center">{{$report->unit_price}}</td>
                                    <?php $found = 1; ?>
                                @endif
                            @endforeach
                        @endif
                        @if($found == 0)
                            <td></td>
                        @endif
                    @endfor
                </tr>
                <tr>
                    <td>当番者</td>
                    @for($i = 1;$i <= $dayCount;$i++)
                        <?php $found = 0; ?>
                        @if($reports)
                            @foreach($reports as $report)
                                @if(\Carbon\Carbon::parse($report->date)->day == $i)
                                    @if($members)
                                        @foreach($members as $member)
                                            @if($member->id == $report->member_id)
                                                <td align="center" valign="center">{{$member->last_name}} {{$member->first_name}}</td>
                                                <?php $found = 1; ?>
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endif
                        @if($found == 0)
                            <td></td>
                        @endif
                    @endfor
                </tr>
            </tbody>
        </table>
    </div>
    @if($nothing == 0)
        <div style="color:red;font-size:15px;font-weight:bold;padding-top:20px;">表示できるデータがありません。物件もしくは表示月を変更し、再度ご検索ください。</div>
    @endif
@stop