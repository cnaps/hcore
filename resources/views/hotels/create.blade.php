@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/hotels">
        <div class="form-group">
            <label>ホテルグループ名：</label>
            <input name="name" type="text" class="form-control" placeholder="ホテルグループ名を入力して下さい。">
        </div>
        <div class="form-group">
            <label>ホテルグループ名（かな）：</label>
            <input name="name_kana" type="text" class="form-control" placeholder="ホテルグループ名（かな）を入力して下さい。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('hotels.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop