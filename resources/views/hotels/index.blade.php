@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('hotels.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ホテルID</th>
                <th>ホテルグループ名</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($hotels)
            @foreach($hotels as $hotel)

                <tr>
                    <td>{{$hotel->id}}</td>
                    <td>{{$hotel->name}}</td>
                    <td>{{$hotel->name_kana}}</td>
                    <td>{{$hotel->updated_at->diffForhumans()}}</td>
                    <td>{{$hotel->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('hotels.show', $hotel->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('hotels.edit', $hotel->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop