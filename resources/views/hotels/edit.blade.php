@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/hotels/{{$hotel->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>ホテルグループ名：</label>
            <input name="name" type="text" class="form-control" value="{{$hotel->name}}" placeholder="物件名を入力して下さい。">
        </div>
        <div class="form-group">
            <label>ホテルグループ名（かな）：</label>
            <input name="name_kana" type="text" class="form-control" value="{{$hotel->name_kana}}" placeholder="物件名（かな）を入力して下さい。">
        </div>
        <div class="pull-right">
            <a href="{{route('hotels.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('hotels.show', $hotel->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop