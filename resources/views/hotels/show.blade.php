@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>ホテルグループID</th>
                <td>{{$hotel->id}}</td>
            </tr>
            <tr>
                <th>ホテルグループ名</th>
                <td>{{$hotel->name}}</td>
            </tr>
            <tr>
                <th>ホテルグループ名（かな）</th>
                <td>{{$hotel->name_kana}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$hotel->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$hotel->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('hotels.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('hotels.edit', $hotel->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop