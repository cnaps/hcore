@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('times.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>打刻ID</th>
                <th>名前</th>
                <th>日付</th>
                <th>時間</th>
                <th>開始/終了</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($times)
            @foreach($times as $time)
                <tr>
                    <td>{{$time->id}}</td>
                    <td>{{$time->member->last_name}} {{$time->member->first_name}}</td>
                    <td>{{$time->date}}</td>
                    <td>{{$time->time}}</td>
                    <td>@if ($time->in_out == "out")終了@else開始@endif</td>
                    <td>{{$time->updated_at->diffForhumans()}}</td>
                    <td>{{$time->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('times.show', $time->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('times.edit', $time->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop