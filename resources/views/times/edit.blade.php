@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/times/{{$time->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>名前：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == old('member_id'))
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" value="{{$time->date}}" type="date" class="form-control">
        </div>
        <div class="form-group">
            <label>時刻：</label>
            <input name="time" value="{{$time->time}}" type="time" class="form-control">
        </div>
        <div class="form-group">
            <label>開始/終了：</label>
            <select name="in_out" class="form-control">
                @if ($time->in_out == "out")
                    <option value="in">開始</option>
                    <option value="out" selected="selected">終了</option>
                @else
                    <option value="in" selected="selected">開始</option>
                    <option value="out">終了</option>
                @endif
            </select>
        </div>
        <div class="pull-right">
            <a href="{{route('notes.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('times.show', $time->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop