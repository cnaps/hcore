@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>打刻ID</th>
                <td>{{$time->id}}</td>
            </tr>
            <tr>
                <th>名前</th>
                <td>{{$time->member->last_name}} {{$time->member->last_name}}</td>
            </tr>
            <tr>
                <th>日付</th>
                <td>{{$time->date}}</td>
            </tr>
            <tr>
                <th>時間</th>
                <td>{{$time->time}}</td>
            </tr>
                <th>開始/終了</th>
                <td>@if ($time->in_out == "out")終了@else開始@endif</td>
            <tr>
                <th>更新日時</th>
                <td>{{$time->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$time->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('times.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('times.edit', $time->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop