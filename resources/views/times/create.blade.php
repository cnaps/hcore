@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/times">
        <div class="form-group">
            <label>名前：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>日付：</label>
            <input name="date" value="<?php use Carbon\Carbon;echo Carbon::now()->format('Y-m-d') ?>" type="date" class="form-control">
        </div>
        <div class="form-group">
            <label>時刻：</label>
            <input name="time" value="<?php echo Carbon::now()->format('H:i:s') ?>" type="time" class="form-control">
        </div>
        <div class="form-group">
            <label>開始/終了：</label>
            <select name="in_out" class="form-control">
                <option value="in">開始</option>
                <option value="out">終了</option>
            </select>
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('times.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop