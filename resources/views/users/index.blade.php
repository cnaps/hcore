@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>アカウントID</th>
                <th>ユーザー名</th>
                <th>メールアドレス</th>
                <th>権限</th>
                <th>所属物件</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($users)
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    @if($authorities)
                        @foreach($authorities as $authority)
                            @if($authority->id == $user->authority_id)
                                <td>{{$authority->name}}</td>
                            @endif
                        @endforeach
                    @endif
                    @if($places)
                        @foreach($places as $place)
                            @if($place->id == $user->place_id)
                                <td>{{$place->name}}</td>
                            @endif
                        @endforeach
                    @endif
                    <td>{{$user->updated_at->diffForhumans()}}</td>
                    <td>{{$user->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('users.edit', $user->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('edit_pass', $user->id)}}" class="btn btn-primary" role="button">パス変更</a>
                        </div>
                    </td>
                    <td><div class="pull-left">
                            <form method="post" action="{{route('users.show', $user->id)}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger">削除</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop