@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/update_pass/{{$user->id}}">
        <div class="form-group">
            <label>ユーザー名：</label>
            <p>{{$user->name}}</p>
        </div>
        <div class="form-group">
            <label>メールアドレス：</label>
            <p>{{$user->email}}</p>
        </div>
        <div class="form-group">
            <label>新しいパスワード：</label>
            <input type="password" name="password" class="form-control"/>
        </div>
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="pull-right">
            <a href="{{route('users.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
@stop