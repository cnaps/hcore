@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/users/{{$user->id}}">
        <div class="form-group">
            <label>ユーザー名：</label>
            <input name="name" type="text" class="form-control" value="{{$user->name}}" placeholder="ユーザー名を入力してください。">
        </div>
        <div class="form-group">
            <label>メールアドレス：</label>
            <input name="email" type="email" class="form-control" value="{{$user->email}}" placeholder="メールアドレスを入力してください。">
        </div>
        <div class="form-group">
            <label>権限：</label>
            <select name="authority_id" class="form-control">
                @if($authorities)
                    @foreach($authorities as $authority)
                        <option value="{{$authority->id}}"
                                @if ($authority->id == $user->authority_id)
                                selected="selected"
                                @endif
                        >{{$authority->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>所属物件：</label>
            <select name="place_id" class="form-control">
                @if($places)
                    @foreach($places as $place)
                        <option value="{{$place->id}}"
                                @if ($place->id == $user->place_id)
                                selected="selected"
                                @endif
                        >{{$place->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="pull-right">
            <a href="{{route('users.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('users.show', $user->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop