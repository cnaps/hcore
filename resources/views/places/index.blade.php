@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('places.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>物件ID</th>
                <th>ホテル</th>
                <th>物件名</th>
                <th>物件名（かな）</th>
                <th>位置</th>
                <th>住所</th>
                <th>室単価</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($places)
            @foreach($places as $place)
                <tr>
                    <td>{{$place->id}}</td>
                    <td>{{$place->hotel->name}}</td>
                    <td>{{$place->name}}</td>
                    <td>{{$place->name_kana}}</td>
                    <td>{{$place->position}}</td>
                    <td>{{$place->address}}</td>
                    <td>{{$place->unit_price}}</td>
                    <td>{{$place->updated_at->diffForhumans()}}</td>
                    <td>{{$place->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('places.show', $place->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('places.edit', $place->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop