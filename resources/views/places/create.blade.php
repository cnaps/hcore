@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/places">
        <div class="form-group">
            <label>ホテルグループ：</label>
            <select name="hotel_id" class="form-control">
                @if($hotels)
                    @foreach($hotels as $hotel)
                        <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>物件名：</label>
            <input name="name" type="text" class="form-control" placeholder="物件名を入力して下さい。">
        </div>
        <div class="form-group">
            <label>物件名（かな）：</label>
            <input name="name_kana" type="text" class="form-control" placeholder="物件名（かな）を入力して下さい。">
        </div>
        <div class="form-group">
            <label>緯度経度（位置）：</label>
            <input name="position" type="text" class="form-control" placeholder="緯度経度を入力して下さい。">
        </div>
        <div class="form-group">
            <label>住所：</label>
            <input name="address" type="text" class="form-control" placeholder="住所を入力して下さい。">
        </div>
        <div class="form-group">
            <label>室単価：</label>
            <input name="unit_price" type="number" class="form-control" placeholder="室単価を入力して下さい。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('places.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop