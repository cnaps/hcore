<!doctype html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <!-- InternetExplorerのブラウザではバージョンによって崩れることがあるので、互換表示をさせないために設定するmetaタグです。 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- レスポンシブWebデザインを使うために必要なmetaタグです。 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="//unpkg.com/leaflet@1.2.0/dist/leaflet.css"
          integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
          crossorigin=""/>
    <link rel="apple-touch-icon" sizes="180x180" href="./img/apple-touch-icon.png">
    <!--<link rel="icon" type="image/png" sizes="32x32" href="./img/favicon-32x32.png">-->
    <link rel="icon" type="image/png" sizes="16x16" href="./img/favicon-16x16.png">
    <link rel="manifest" href="./img/manifest.json">
    <link rel="mask-icon" href="./img/safari-pinned-tab.svg" color="#276196">
    <link rel="shortcut icon" href="./img/favicon.ico">
    <meta name="msapplication-config" content="./img/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <title>H-CORE</title>
    <style>
        html, body, #mapid {
            height: 100%;
            margin: 0px;
            padding: 0px
        }
    </style>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="//unpkg.com/leaflet@1.2.0/dist/leaflet.js"
            integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
            crossorigin=""></script>

</head>

<body>
<div id="mapid"></div>
<script>
    var Icon = L.icon({
        iconUrl: 'img/pin.png',
        shadowUrl: 'img/pins.png',

        iconSize:     [38, 48], // size of the icon
        shadowSize:   [40, 34], // size of the shadow
        iconAnchor:   [19, 50], // point of the icon which will correspond to marker's location
        shadowAnchor: [3, 34],  // the same for the shadow
        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
    var IconY = L.icon({
        iconUrl: 'img/pin_y.png',
        shadowUrl: 'img/pins.png',

        iconSize:     [38, 48], // size of the icon
        shadowSize:   [40, 34], // size of the shadow
        iconAnchor:   [19, 50], // point of the icon which will correspond to marker's location
        shadowAnchor: [3, 34],  // the same for the shadow
        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });
    var IconR = L.icon({
        iconUrl: 'img/pin_r.png',
        shadowUrl: 'img/pins.png',

        iconSize:     [38, 48], // size of the icon
        shadowSize:   [40, 34], // size of the shadow
        iconAnchor:   [19, 50], // point of the icon which will correspond to marker's location
        shadowAnchor: [3, 34],  // the same for the shadow
        popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    window.onload = function () {
        // 地図のデフォルトの緯度経度と拡大率
        // 適当に日本の真ん中あたり(35.5, 137)をZoomレベル5で
        var mapid = L.map('mapid').setView([35.5,137], 7);

        var tileLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
            attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
            maxZoom: 19
        });
        tileLayer.addTo(mapid);

        @if($places)
            @foreach($places as $place)
                <?php $mCount = 0; ?>
                <?php $done = 0; ?>
                <?php $oneYearRoom = 0; ?>
                <?php $oneYearOrder = 0; ?>
                <?php $oneYearFinish = 0; ?>
                <?php $oneMonthRoom = 0; ?>
                <?php $oneMonthOrder = 0; ?>
                <?php $oneMonthFinish = 0; ?>
                <?php $yesterdayRoom = 0; ?>
                <?php $yesterdayOrder = 0; ?>
                <?php $yesterdayFinish = 0; ?>
                <?php $RoomCount = 0; ?>
                @if($room_counts)
                    @foreach($room_counts as $room_count)
                        @if($room_count->place_id == $place->id)
                            <?php $RoomCount = $room_count["count"]; ?>
                            <?php $done = 1; ?>
                        @endif
                    @endforeach
                @endif
                @if($rooms)
                    @foreach($rooms as $room)
                        @if($room->place_id == $place->id)
                            @if($oneYearTasks)
                                @foreach($oneYearTasks as $oneYearTask)
                                    @if($room->id == $oneYearTask->room_id)
                                        <?php $oneYearRoom++; ?>
                                        @if($oneYearTask->order_flg == 1)
                                            <?php $oneYearOrder++; ?>
                                        @endif
                                        @if($oneYearTask->finish_flg == 1)
                                            <?php $oneYearFinish++; ?>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                @endif
                @if($rooms)
                    @foreach($rooms as $room)
                        @if($room->place_id == $place->id)
                            @if($oneMonthTasks)
                                @foreach($oneMonthTasks as $oneMonthTask)
                                    @if($room->id == $oneYearTask->room_id)
                                        <?php $oneMonthRoom++; ?>
                                        @if($oneMonthTask->order_flg == 1)
                                            <?php $oneMonthOrder++; ?>
                                        @endif
                                        @if($oneMonthTask->finish_flg == 1)
                                            <?php $oneMonthFinish++; ?>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                @endif
                @if($rooms)
                    @foreach($rooms as $room)
                        @if($room->place_id == $place->id)
                            @if($yesterdayTasks)
                                @foreach($yesterdayTasks as $yesterdayTask)
                                    @if($room->id == $yesterdayTask->room_id)
                                        <?php $yesterdayRoom++; ?>
                                        @if($yesterdayTask->order_flg == 1)
                                            <?php $yesterdayOrder++; ?>
                                        @endif
                                        @if($yesterdayTask->finish_flg == 1)
                                            <?php $yesterdayFinish++; ?>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        @endif
                    @endforeach
                @endif
                @if($oneYearRoom != 0)
                    <?php $oneYearCal = round($oneYearFinish / $oneYearRoom * 100,1); ?>
                @endif
                @if($oneMonthRoom != 0)
                    <?php $oneMonthCal = round($oneMonthFinish / $oneMonthRoom * 100,1); ?>
                @endif
                @if($yesterdayRoom != 0)
                    <?php $yesterdayCal = round($yesterdayFinish / $yesterdayRoom * 100,1); ?>
                @endif
                @foreach($members as $member)
                    @if($member->place_id == $place->id)
                        @if($member->shift_monday == 1)
                            <?php $mCount++; ?>
                        @endif
                        @if($member->shift_tuesday == 1)
                            <?php $mCount++; ?>
                        @endif
                        @if($member->shift_wednesday == 1)
                            <?php $mCount++; ?>
                        @endif
                        @if($member->shift_thursday == 1)
                            <?php $mCount++; ?>
                        @endif
                        @if($member->shift_friday == 1)
                            <?php $mCount++; ?>
                        @endif
                        @if($member->shift_saturday == 1)
                            <?php $mCount++; ?>
                        @endif
                        @if($member->shift_sunday == 1)
                            <?php $mCount++; ?>
                        @endif
                    @endif
                @endforeach
                @if($mCount != 0)
                <?php
                $RoomCount = $RoomCount * 7;
                if ($RoomCount != 0){
                    $room12 = round($mCount * 12 * 100 / $RoomCount,1);
                    $room15 = round($mCount * 15 * 100 / $RoomCount,1);
                }else{
                    $room12 = 0;
                    $room15 = 0;
                }
                ?>
                @endif
                @if ($place->position != null)
                    @if($room15 < $oneMonthCal)
                        var marker{{$place->id}} = L.marker([<?php echo $place["position"]; ?>], {icon: IconR}).addTo(mapid);
                    @elseif($room12 < $oneMonthCal)
                        var marker{{$place->id}} = L.marker([<?php echo $place["position"]; ?>], {icon: IconY}).addTo(mapid);
                    @else
                        var marker{{$place->id}} = L.marker([<?php echo $place["position"]; ?>], {icon: Icon}).addTo(mapid);
                    @endif
                    var position = <?php echo json_encode($place["position"]); ?>;
                    marker{{$place->id}}.bindPopup("" +
                        '<b style="font-size:200%;text-align:center">{{$place->name}}</b><br>' +
                        '<p>登録室数：</p><p style="font-size:200%;text-align:center;font-weight:bold">{{$RoomCount}}</p>' +
                        '<p>過去1ヵ月間の稼働率：</p><p style="font-size:200%;text-align:center;font-weight:bold">{{$oneMonthCal}}%</p>' +
                        '<p>処理可能稼働率（12室/人）：</p><p style="font-size:200%;text-align:center;font-weight:bold">{{$room12}}%</p>' +
                        '<p>処理可能稼働率（15室/人）：</p><p style="font-size:200%;text-align:center;font-weight:bold">{{$room15}}%</p>' +
                        '<a href="https://maps.google.com/maps?q='+ position+'" target="_blank">GoogleMapで場所を表示</a>' +
                        "").openPopup();
                @endif
            @endforeach
        @endif
        }
</script>


</body>

</html>