@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>物件ID</th>
                <td>{{$place->id}}</td>
            </tr>
            <tr>
                <th>ホテル</th>
                <td>{{$place->hotel->name}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$place->name}}</td>
            </tr>
            <tr>
                <th>物件名（かな）</th>
                <td>{{$place->name_kana}}</td>
            </tr>
            <tr>
                <th>位置</th>
                <td>{{$place->position}}</td>
            </tr>
            <tr>
                <th>住所</th>
                <td>{{$place->address}}</td>
            </tr>
            <tr>
                <th>室単価</th>
                <td>{{$place->unit_price}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$place->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$place->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('places.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('places.edit', $place->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop