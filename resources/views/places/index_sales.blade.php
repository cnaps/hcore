@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div style="overflow-x: scroll;-webkit-overflow-scrolling: touch;">
        <table class="table table-striped" style="width:2000px;">
            <thead>
                <tr>
                    <th>物件ID</th>
                    <th>ホテル</th>
                    <th>物件名</th>
                    <th>登録部屋数</th>
                    <th>年間稼働率</th>
                    <!--<th>年間取扱い部屋数</th>
                    <th>年間指示部屋数</th>
                    <th>年間清掃部屋実数</th>-->
                    <th>月間稼働率</th>
                    <!--<th>月間取扱い部屋数</th>
                    <th>月間指示部屋数</th>
                    <th>月間清掃部屋実数</th>-->
                    <th>昨日稼働率</th>
                    <th>一週間の出勤人数</th>
                    <th>対応可能稼働率<br/>（12室/人）</th>
                    <th>対応可能稼働率<br/>（15室/人）</th>
                </tr>
            </thead>
            <tbody>
            @if($places)
                @foreach($places as $place)
                    <?php $mCount = 0; ?>
                    <?php $done = 0; ?>
                    <?php $oneYearRoom = 0; ?>
                    <?php $oneYearOrder = 0; ?>
                    <?php $oneYearFinish = 0; ?>
                    <?php $oneMonthRoom = 0; ?>
                    <?php $oneMonthOrder = 0; ?>
                    <?php $oneMonthFinish = 0; ?>
                    <?php $yesterdayRoom = 0; ?>
                    <?php $yesterdayOrder = 0; ?>
                    <?php $yesterdayFinish = 0; ?>
                    <?php $RoomCount = 0; ?>
                    <tr>
                        <td>{{$place->id}}</td>
                        <td>{{$place->hotel->name}}</td>
                        <td>{{$place->name}}</td>
                        @if($room_counts)
                            @foreach($room_counts as $room_count)
                                @if($room_count->place_id == $place->id)
                                    <td>{{$room_count->count}}</td>
                                    <?php $RoomCount = $room_count["count"]; ?>
                                    <?php $done = 1; ?>
                                @endif
                            @endforeach
                            @if($done == 0)
                                <td></td>
                            @endif
                        @else
                            <td></td>
                        @endif


                        @if($rooms)
                            @foreach($rooms as $room)
                                @if($room->place_id == $place->id)
                                    @if($oneYearTasks)
                                        @foreach($oneYearTasks as $oneYearTask)
                                            @if($room->id == $oneYearTask->room_id)
                                                <?php $oneYearRoom++; ?>
                                                @if($oneYearTask->order_flg == 1)
                                                    <?php $oneYearOrder++; ?>
                                                @endif
                                                @if($oneYearTask->finish_flg == 1)
                                                    <?php $oneYearFinish++; ?>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endif

                        @if($rooms)
                            @foreach($rooms as $room)
                                @if($room->place_id == $place->id)
                                    @if($oneMonthTasks)
                                        @foreach($oneMonthTasks as $oneMonthTask)
                                            @if($room->id == $oneYearTask->room_id)
                                                <?php $oneMonthRoom++; ?>
                                                @if($oneMonthTask->order_flg == 1)
                                                    <?php $oneMonthOrder++; ?>
                                                @endif
                                                @if($oneMonthTask->finish_flg == 1)
                                                    <?php $oneMonthFinish++; ?>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endif

                        @if($rooms)
                            @foreach($rooms as $room)
                                @if($room->place_id == $place->id)
                                    @if($yesterdayTasks)
                                        @foreach($yesterdayTasks as $yesterdayTask)
                                            @if($room->id == $yesterdayTask->room_id)
                                                <?php $yesterdayRoom++; ?>
                                                @if($yesterdayTask->order_flg == 1)
                                                    <?php $yesterdayOrder++; ?>
                                                @endif
                                                @if($yesterdayTask->finish_flg == 1)
                                                    <?php $yesterdayFinish++; ?>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                @endif
                            @endforeach
                        @endif

                        <!--年間稼働率-->
                        @if($oneYearRoom != 0)
                            <?php $cal = round($oneYearFinish / $oneYearRoom * 100,1); ?>
                            <td>{{$cal}}%</td>
                        @else
                            <td>0%</td>
                        @endif
                        <!--年間取り扱い部屋数延べ数-->
                        {{--<td>{{$oneYearRoom}}</td>--}}
                        <!--年間指示部屋数-->
                        {{--<td>{{$oneYearOrder}}</td>--}}
                        <!--年間清掃部屋実数-->
                        {{--<td>{{$oneYearFinish}}</td>--}}

                        <!--月間稼働率-->
                        @if($oneMonthRoom != 0)
                            <?php $cal = round($oneMonthFinish / $oneMonthRoom * 100,1); ?>
                            <td>{{$cal}}%</td>
                        @else
                            <td>0%</td>
                        @endif
                        {{--<!--月間取り扱い部屋数延べ数-->--}}
                        {{--<td>{{$oneMonthRoom}}</td>--}}
                        {{--<!--月間指示部屋数-->--}}
                        {{--<td>{{$oneMonthOrder}}</td>--}}
                        {{--<!--月間清掃部屋実数-->--}}
                        {{--<td>{{$oneMonthFinish}}</td>--}}

                        <!--昨日稼働率-->
                        @if($yesterdayRoom != 0)
                            <?php $cal = round($yesterdayFinish / $yesterdayRoom * 100,1); ?>
                            <td>{{$cal}}%</td>
                        @else
                            <td>0%</td>
                        @endif

                        @foreach($members as $member)
                            @if($member->place_id == $place->id)
                                @if($member->shift_monday == 1)
                                    <?php $mCount++; ?>
                                @endif
                                @if($member->shift_tuesday == 1)
                                    <?php $mCount++; ?>
                                @endif
                                @if($member->shift_wednesday == 1)
                                    <?php $mCount++; ?>
                                @endif
                                @if($member->shift_thursday == 1)
                                    <?php $mCount++; ?>
                                @endif
                                @if($member->shift_friday == 1)
                                    <?php $mCount++; ?>
                                @endif
                                @if($member->shift_saturday == 1)
                                    <?php $mCount++; ?>
                                @endif
                                @if($member->shift_sunday == 1)
                                    <?php $mCount++; ?>
                                @endif
                            @endif
                        @endforeach
                        <!--一週間の出勤人数-->
                        <td>{{$mCount}}</td>
                        <?php $room12 = 0; ?>
                        <?php $room15 = 0; ?>
                        @if($mCount != 0 || $mCount != null)
                        <?php
                            $RoomCount = $RoomCount * 7;
                            if ($RoomCount != 0){
                                $room12 = round($mCount * 12 * 100 / $RoomCount,1);
                                $room15 = round($mCount * 15 * 100 / $RoomCount,1);
                            }else{
                                $room12 = 0;
                                $room15 = 0;
                            }
                            ?>
                            <td>{{$room12}}%</td>
                            <td>{{$room15}}%</td>
                        @else
                            <td>0%</td>
                            <td>0%</td>
                        @endif
                        <!--対応可能稼働率-->
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@stop