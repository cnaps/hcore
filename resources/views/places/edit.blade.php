@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/places/{{$place->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>ホテルグループ：</label>
            <select name="hotel_id" class="form-control">
                @if($hotels)
                    @foreach($hotels as $hotel)
                        <option value="{{$hotel->id}}"
                                @if ($hotel->id == old('hotel_id'))
                                selected="selected"
                                @endif
                        >{{$hotel->name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>物件名：</label>
            <input name="name" type="text" class="form-control" value="{{$place->name}}" placeholder="物件名を入力して下さい。">
        </div>
        <div class="form-group">
            <label>物件名（かな）：</label>
            <input name="name_kana" type="text" class="form-control" value="{{$place->name_kana}}" placeholder="物件名（かな）を入力して下さい。">
        </div>
        <div class="form-group">
            <label>緯度経度（位置）：</label>
            <input name="position" type="text" class="form-control" value="{{$place->position}}" placeholder="緯度経度を入力して下さい。">
        </div>
        <div class="form-group">
            <label>住所：</label>
            <input name="address" type="text" class="form-control" value="{{$place->address}}" placeholder="住所を入力して下さい。">
        </div>
        <div class="form-group">
            <label>室単価：</label>
            <input name="unit_price" type="number" class="form-control" value="{{$place->unit_price}}" placeholder="室単価を入力して下さい。">
        </div>
        <div class="pull-right">
            <a href="{{route('places.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('places.show', $place->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop