@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/statuses">
        <div class="form-group">
            <label>勤務形態：</label>
            <input name="name" type="text" class="form-control" placeholder="勤務形態を入力してください。">
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('statuses.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop