@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>投稿ID</th>
                <td>{{$note->id}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$note->place->name}}</td>
            </tr>
            <tr>
                <th>タイトル</th>
                <td>{{$note->title}}</td>
            </tr>
            <tr>
                <th>投稿内容</th>
                <td>{{$note->note}}</td>
            </tr>
            <tr>
                <th>報告者</th>
                <td>{{$note->member->last_name}} {{$note->member->first_name}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$note->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$note->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('notes.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('notes.edit', $note->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop