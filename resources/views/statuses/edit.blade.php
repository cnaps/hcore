@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/statuses/{{$status->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>勤務形態：</label>
            <input name="name" value="{{$status->name}}" type="text" class="form-control" placeholder="勤務形態を入力してください。">
        </div>
        <div class="pull-right">
            <a href="{{route('statuses.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('statuses.show', $status->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop