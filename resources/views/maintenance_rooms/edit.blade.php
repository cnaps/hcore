@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/maintenance_rooms/{{$maintenance_room->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>部屋番号：</label>
            <select name="room_id" class="form-control">
                @if($rooms)
                    @foreach($rooms as $room)
                        <option value="{{$room->id}}"
                                @if ($room->id == old('room_id'))
                                selected="selected"
                                @endif
                        >{{$room->room}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>修理内容：</label>
            <textarea name="note" value="{{$maintenance_room->note}}" placeholder="修理が必要な箇所に関して詳細を記述してください。" rows="4" class="form-control" id="InputTextarea"></textarea>
        </div>
        <div class="form-group">
            <label>状態：</label>
            <select name="fixed" class="form-control">
                @if ($maintenance_room->fixed == "true")
                    <option value="false">未処理</option>
                    <option value="true" selected="selected">処理済み</option>
                @else
                    <option value="false" selected="selected">未処理</option>
                    <option value="true">処理済み</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>報告者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == old('member_id'))
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="pull-right">
            <a href="{{route('maintenance_rooms.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('maintenance_rooms.show', $maintenance_room->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop