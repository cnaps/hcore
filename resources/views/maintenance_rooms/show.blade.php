@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>修理部屋ID</th>
                <td>{{$maintenance_room->id}}</td>
            </tr>
            <tr>
                <th>物件名</th>
                <td>{{$maintenance_room->room->place->name}}</td>
            </tr>
            <tr>
                <th>部屋番号</th>
                <td>{{$maintenance_room->room->room}}</td>
            </tr>
            <tr>
                <th>内容</th>
                <td>{{$maintenance_room->note}}</td>
            </tr>
            <tr>
                <th>状態</th>
                <td>{{$maintenance_room->fixed}}</td>
            </tr>
            <tr>
                <th>報告者</th>
                <td>{{$maintenance_room->member->last_name}} {{$maintenance_room->member->first_name}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$maintenance_room->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$maintenance_room->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('maintenance_rooms.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('maintenance_rooms.edit', $maintenance_room->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop