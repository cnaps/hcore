@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/maintenance_rooms">
        <div class="form-group">
            <label>部屋番号：</label>
            <select name="room_id" class="form-control">
                @if($rooms)
                    @foreach($rooms as $room)
                        <option value="{{$room->id}}">{{$room->room}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>修理内容：</label>
            <textarea name="note" placeholder="修理が必要な箇所に関して詳細を記述してください。" rows="4" class="form-control" id="InputTextarea"></textarea>
        </div>
        <div class="form-group">
            <label>状態：</label>
            <select name="fixed" class="form-control">
                <option value="false">未処理</option>
                <option value="true">処理済み</option>
            </select>
        </div>
        <div class="form-group">
            <label>報告者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('maintenance_rooms.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop