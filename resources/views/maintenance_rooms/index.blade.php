@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('maintenance_rooms.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>修理部屋ID</th>
                <th>物件名</th>
                <th>部屋番号</th>
                <th>内容</th>
                <th>状態</th>
                <th>報告者</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($maintenance_rooms)
            @foreach($maintenance_rooms as $maintenance_room)

                <tr>
                    <td>{{$maintenance_room->id}}</td>
                    <td>{{$maintenance_room->room->place->name}}</td>
                    <td>{{$maintenance_room->room->room}}</td>
                    <td>{{$maintenance_room->note}}</td>
                    <td>{{$maintenance_room->fixed}}</td>
                    <td>{{$maintenance_room->member->last_name}} {{$maintenance_room->member->first_name}}</td>
                    <td>{{$maintenance_room->updated_at->diffForhumans()}}</td>
                    <td>{{$maintenance_room->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('maintenance_rooms.show', $maintenance_room->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('maintenance_rooms.edit', $maintenance_room->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop