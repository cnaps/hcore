@extends('layouts.app')
@section('content')
    <h1>新規登録</h1>
    <form method="post" action="/replies">
        <div class="form-group">
            <label>返信する記事：</label>
            <select name="note_id" class="form-control">
                @if($notes)
                    @foreach($notes as $note)
                        <option value="{{$note->id}}">{{$note->title}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>返信内容：</label>
            <textarea name="note_reply" placeholder="返信内容を入力してください。" rows="4" class="form-control" id="InputTextarea"></textarea>
        </div>
        <div class="form-group">
            <label>投稿者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        {{csrf_field()}}
        <div class="pull-right">
            <a href="{{route('replies.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">登録</button>
        </div>
    </form>
@stop