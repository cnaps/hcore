@extends('layouts.app')
@section('content')
    <h1>一覧</h1>
    <div class="pull-right">
        <a href="{{route('replies.create')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>返信ID</th>
                <th>返信する投稿</th>
                <th>返信内容</th>
                <th>投稿者</th>
                <th>更新日時</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($replies)
            @foreach($replies as $reply)
                <tr>
                    <td>{{$reply->id}}</td>
                    <td>{{$reply->note->title}}</td>
                    <td>{{$reply->note_reply}}</td>
                    <td>{{$reply->member->last_name}} {{$reply->member->first_name}}</td>
                    <td>{{$reply->updated_at->diffForhumans()}}</td>
                    <td>{{$reply->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('replies.show', $reply->id)}}" class="btn btn-default" role="button">詳細</a>
                            <a href="{{route('replies.edit', $reply->id)}}" class="btn btn-primary" role="button">変更</a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop