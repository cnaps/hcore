@extends('layouts.app')
@section('content')
    <h1>登録変更</h1>
    <form method="post" action="/replies/{{$reply->id}}">
        {{csrf_field()}}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <label>返信する記事：</label>
            <select name="note_id" class="form-control">
                @if($notes)
                    @foreach($notes as $note)
                        <option value="{{$note->id}}"
                                @if ($note->id == old('note_id'))
                                selected="selected"
                                @endif
                        >{{$note->title}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group">
            <label>返信内容：</label>
            <textarea name="note_reply" placeholder="返信内容を記述してください。" rows="4" class="form-control" id="InputTextarea">{{$reply->note_reply}}</textarea>
        </div>
        <div class="form-group">
            <label>投稿者：</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}"
                                @if ($member->id == old('member_id'))
                                selected="selected"
                                @endif
                        >{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="pull-right">
            <a href="{{route('replies.index')}}" class="btn btn-default" role="button">戻る</a>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
    <div class="pull-left">
        <form method="post" action="{{route('replies.show', $reply->id)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">削除</button>
        </form>
    </div>
@stop