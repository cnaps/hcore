@extends('layouts.app')
@section('content')
    <h1>伝言ノート</h1>
    <div class="pull-right">
        <a href="{{route('create_master_note')}}" class="btn btn-primary" role="button">新規登録</a>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>タイトル</th>
                <th>登録日時</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        @if($notes)
            @foreach($notes as $note)
                <tr>
                    <td>
                        @if($note->place_id == 1)
                            <div style="text-align:center;padding:7px;border-radius: 3px;background-color: #337ab7;color:#ffffff">{{$note->place->name}}</div>
                        @else
                            <div style="text-align:center;padding:7px;border-radius: 3px;color:#337ab7;border:1px solid #337ab7">{{$note->place->name}}</div>
                        @endif
                    </td>
                    <td style="font-weight: bold">
                        <a href="{{route('show_note', $note->id)}}" style="font-weight: bold">{{$note->title}}</a>
                    </td>
                    <td>{{$note->created_at->diffForhumans()}}</td>
                    <td>
                        <div class="pull-right">
                            <a href="{{route('show_note', $note->id)}}" class="btn btn-default" role="button">詳細</a>
                            {{--<a href="{{route('notes.edit', $note->id)}}" class="btn btn-primary" role="button">変更</a>--}}
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@stop