@extends('layouts.app')
@section('content')
    <a class="btn btn-default" href="{{route('index_note')}}">戻る</a>
    <hr>
    <h1>{{$note->title}}</h1>
    <p class="text-right"><strong>{{$place->name}}</strong> {{$member->last_name}} {{$member->first_name}} <span style="padding-left: 20px; color: #999999">{{$note->created_at->diffForhumans()}}</span></p>
    <div style="padding: 20px;background-color: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;color: #333;">{{$note->note}}</div>
    <hr>
    <form method="post" action="/store_note">
        {{csrf_field()}}
        <input name="note_id" type="hidden" value="{{$id}}">
        <div class="form-group">
            <label>名前</label>
            <select name="member_id" class="form-control">
                @if($members)
                    @foreach($members as $member)
                        <option value="{{$member->id}}">{{$member->last_name}} {{$member->first_name}}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="form-group" style="margin-bottom: 0">
            <label>コメント</label>
            <textarea name="note_reply" placeholder="追加するコメントを入力してください。" rows="3" class="form-control" id="InputTextarea"></textarea>
        </div>
        <button type="submit" class="btn btn-primary pull-right" style="margin-top: 20px">追 加</button>
    </form>
    <div style="margin-top: 80px">
    @if($replies)
        @foreach($replies as $reply)
            <div style="width:100%;margin-top: 20px;margin-bottom: 40px">
                <p style="font-size: 13px"><strong>{{$reply->member->place->name}}</strong> {{$reply->member->last_name}} {{$reply->member->first_name}} <span style="color: #999999;padding-left: 20px">{{$reply->created_at->diffForhumans()}}</span></p>
                <div>
                    {{$reply->note_reply}}
                </div>
            </div>
        @endforeach
    @endif
    </div>
@stop
