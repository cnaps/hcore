@extends('layouts.app')
@section('content')
    <h1>詳細</h1>
    <table class="table">
        <tbody>
            <tr>
                <th>返信ID</th>
                <td>{{$reply->id}}</td>
            </tr>
            <tr>
                <th>返信する投稿</th>
                <td>{{$reply->note->title}}</td>
            </tr>
            <tr>
                <th>返信内容</th>
                <td>{{$reply->note_reply}}</td>
            </tr>
            <tr>
                <th>報告者</th>
                <td>{{$reply->member->last_name}} {{$reply->member->first_name}}</td>
            </tr>
            <tr>
                <th>更新日時</th>
                <td>{{$reply->updated_at->diffForhumans()}}</td>
            </tr>
            <tr>
                <th>登録日時</th>
                <td>{{$reply->created_at->diffForhumans()}}</td>
            </tr>
        </tbody>
    </table>
    <div class="pull-right">
        <a href="{{route('notes.index')}}" class="btn btn-default" role="button">戻る</a>
        <a href="{{route('notes.edit', $reply->id)}}" class="btn btn-primary" role="button">変更</a>
    </div>
@stop