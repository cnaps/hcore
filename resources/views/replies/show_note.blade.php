@extends('layouts.app')
@section('content')
    <a class="btn btn-default" href="{{route('index_note')}}">戻る</a>
    <hr>
    <h1>{{$note->title}}</h1>
    <p class="text-right"><strong>{{$place->name}}</strong> {{$member->last_name}} {{$member->first_name}} <span style="padding-left: 20px; color: #999999">{{$note->created_at->diffForhumans()}}</span></p>
    <div style="white-space:pre-wrap;padding: 20px;background-color: #f5f5f5;border: 1px solid #ccc;border-radius: 4px;color: #333;">{{$note->note}}</div>
    <div style="margin-top: 20px">
        @if($place_id == $note->place_id)
            <form method="post" action="{{route('delete_master_note', $note->id)}}">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger">削除</button>
            </form>
        @endif
    </div>
    <div class="pull-right">
        <a class="btn btn-primary text-right" href="{{route('create_note', $note->id)}}" style="margin-top: 20px">コメントを追加する</a>
    </div>
    <div style="margin-top: 80px">
    @if($replies)
        @foreach($replies as $reply)
            <div style="width:100%;margin-top: 20px;margin-bottom: 40px">
                <p style="font-size: 13px"><strong>{{$reply->member->place->name}}</strong> {{$reply->member->last_name}} {{$reply->member->first_name}} <span style="color: #999999;padding-left: 20px">{{$reply->created_at->diffForhumans()}}</span></p>
                <div>
                    {{$reply->note_reply}}
                </div>
                @if($place_id == $reply->member->place_id)
                <form method="post" action="{{route('delete_note', $reply->id)}}">
                    {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-default" style="border:none;padding:0;font-size: 10px;text-decoration: underline;color:red;">削除</button>
                </form>
                @endif
            </div>
        @endforeach
    @endif
    </div>
@stop
